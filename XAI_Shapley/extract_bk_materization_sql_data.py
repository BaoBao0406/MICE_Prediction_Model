#! python3
# extract_bk_materization_sql_data.py -

import pyodbc
from datetime import datetime
from dateutil.relativedelta import relativedelta
import pandas as pd



class Date_Range:
    
    def __init__(self):
        # Set date range
        self.now = datetime.now()
        self.start_date = str(self.now.year) + '-' + str(self.now.month) + '-' + str(self.now.day)
        # Arrival date
        self.end_year_arrival = self.now + relativedelta(years=5)
        self.end_date_arrival = str(self.end_year_arrival.year) + '-12-31'
    

class Extract_SQLServer_Data(Date_Range):
    
    def __init__(self):
        super().__init__()
        self.conn = self.server_connection()
        self.user = self.user_data()
        
    def server_connection(self) -> object:
        # Create connection
        self.conn = pyodbc.connect('Driver={SQL Server};'
                                          'Server=VOPPSCLDBN01\VOPPSCLDBI01;'
                                          'Database=SalesForce;'
                                          'Trusted_Connection=yes;')
        return self.conn
        
    def user_data(self) -> dict:
        # FDC User ID and Name list
        self.user = pd.read_sql('SELECT DISTINCT(Id), Name \
                                 FROM dbo.[User]', self.conn)
        self.user = self.user.set_index('Id')['Name'].to_dict()
        
        return self.user
    

    def sql_ml_bk_data(self) -> pd.DataFrame:
        # query ml booking data
        BK_ml_data = pd.read_sql("SELECT ac.Id, ag.Id, BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'yyyy-MM-dd') AS DepartureDate, BK.nihrm__Property__c, ag.nihrm__RegionName__c, ag.Industry, BK.End_User_Region__c, \
                                         BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.RSO_Manager__c, ac.nihrm__RegionName__c, ac.Industry, BK.nihrm__BookingMarketSegmentName__c, BK.Promotion__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, BK.nihrm__BookingStatus__c, \
                                         BK.nihrm__LeadSourceName__c, ac.Name, ag.Name, BK.Name, BK.OwnerId \
                                  FROM dbo.nihrm__Booking__c AS BK \
                                     LEFT JOIN dbo.Account AS ac \
                                         ON BK.nihrm__Account__c = ac.Id \
                                     LEFT JOIN dbo.Account AS ag \
                                         ON BK.nihrm__Agency__c = ag.Id \
                                  WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                        (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.nihrm__BookingStatus__c IN ('Tentative', 'Prospect'))", self.conn)
        BK_ml_data.columns = ['Account', 'Agency', 'Id', 'BK_no', 'ArrivalDate', 'DepartureDate', 'Property', 'Agency: Region', 'Agency: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'RSO Manager', 'Account: Region', 
                              'Account: Industry', 'Market Segment', 'Promotion', 'BookedDate', 'Status', 'Lead Source', 'Account: Name', 'Agency: Name', 'Post As', 'Owner']
        BK_ml_data['RSO Manager'].replace(self.user, inplace=True)
        BK_ml_data['Owner'].replace(self.user, inplace=True)
        
        return BK_ml_data
    
    
    def sql_ml_room_block_data(self) -> pd.DataFrame:
        # query ml room block data
        RoomB_ml_data = pd.read_sql("SELECT RoomB.nihrm__Booking__c, prop.Name, RoomB.nihrm__CurrentBlendedRoomnightsTotal__c, RoomB.nihrm__CurrentBlendedRevenueTotal__c, RoomB.nihrm__CurrentBlendedADR__c, RoomB.nihrm__PeakRoomnightsAgreed__c \
                                     FROM dbo.nihrm__BookingRoomBlock__c AS RoomB \
                                         INNER JOIN dbo.nihrm__Location__c AS prop \
                                             ON RoomB.nihrm__Location__c = prop.Id \
                                     WHERE (RoomB.nihrm__StartDate__c BETWEEN CONVERT(datetime, '" + self.start_date + "') AND CONVERT(datetime, '" + self.end_date_arrival + "')) AND \
                                         (prop.Name NOT IN ('Sands Macao Hotel', 'Sheraton Grand Macao', 'The St. Regis Macao', 'Four Seasons Hotel Macao Cotai Strip'))", self.conn)
        RoomB_ml_data.columns = ['Id', 'Property', 'RNs', 'RNs Revenue', 'ADR', 'Peak RNs']
        RoomB_ml_data = RoomB_ml_data[['Id', 'Property', 'RNs', 'RNs Revenue', 'ADR', 'Peak RNs']]
        RoomB_ml_data = RoomB_ml_data.groupby(['Id', 'Property']).agg({'RNs': 'sum', 'RNs Revenue': 'sum', 'ADR': 'max', 'Peak RNs': 'max'})
        RoomB_ml_data.reset_index(inplace=True)
        
        return RoomB_ml_data
    
    
    def sql_ml_event_data(self) -> pd.DataFrame:
        # query ml event data
        Event_ml_data = pd.read_sql("SELECT ET.nihrm__Booking__c, ET.nihrm__Property__c, MAX(ET.nihrm__AgreedAttendance__c), SUM(ET.nihrm__CurrentBlendedRevenue1__c), SUM(ET.nihrm__CurrentBlendedRevenue2__c), SUM(ET.nihrm__CurrentBlendedRevenue9__c), SUM(ET.nihrm__CurrentBlendedRevenue4__c), SUM(ET.nihrm__CurrentBlendedRevenue7__c) \
                                     FROM dbo.nihrm__BookingEvent__c AS ET \
                                     WHERE (ET.nihrm__StartDate__c BETWEEN CONVERT(datetime, '" + self.start_date + "') AND CONVERT(datetime, '" + self.end_date_arrival + "')) AND \
                                         (ET.nihrm__Property__c NOT IN ('Sands Macao Hotel', 'Sheraton Grand Macao', 'The St. Regis Macao', 'Four Seasons Hotel Macao Cotai Strip')) \
                                     GROUP BY ET.nihrm__Property__c, ET.nihrm__Booking__c", self.conn)
        Event_ml_data.columns = ['Id', 'Property', 'Attendance', 'Food Revenue', 'Beverage Revenue', 'Outlet Revenue', 'AV Revenue', 'Rental Revenue']
        Event_ml_data['F&B Revenue'] = Event_ml_data['Food Revenue'] + Event_ml_data['Beverage Revenue'] + Event_ml_data['Outlet Revenue']
        Event_ml_data = Event_ml_data[['Id', 'Property', 'Attendance', 'F&B Revenue', 'AV Revenue', 'Rental Revenue']]
        
        return Event_ml_data
