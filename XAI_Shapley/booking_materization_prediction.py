#! python3
# booking_materization_prediction.py -

import shutil, re, shap, pickle
import pandas as pd
import matplotlib.pyplot as plt

import extract_bk_materization_sql_data, process_booking_materization_data, booking_materization_prediction_data



def prediction_to_excel(booking_data: pd.DataFrame, cur_path: str, dst_path: str) -> None:

    booking_data.to_excel(cur_path + 'testing_booking_data_checking.xlsx', index=False)
    
#    shutil.copy(cur_path + 'booking_data_checking.xlsx', dst_path)    


class Shapley_Explainer_plot():
    
    def __init__(self, model_path):
        
        self.model_path = model_path
        
        self.load_model()
    
    def load_model(self) -> None:
        
        self.model = open(self.model_path + '\\Model\\' + 'xgb_materization_model.pkl', 'rb')
        self.model = pickle.load(self.model)
    
    def load_explainer_dataset(self, shapley_tf_data: pd.DataFrame):
        
        self.shapley_tf_data = shapley_tf_data.drop(['TD %', 'D %'], axis=1)
        
        new_explainer_dataset = shapley_tf_data.drop(['BK_no', 'TD %', 'D %'], axis=1)
        
        self.explainer_dataset = pd.read_excel(self.model_path + 'metadata\\shapley_explainer_dataset\\xai_explainer_dataset.xlsx')
        self.explainer_dataset = pd.concat([self.explainer_dataset, new_explainer_dataset], axis=0)
        self.explainer_dataset.reset_index(drop=True, inplace=True)
        
        shap_values_id = shapley_tf_data['BK_no']
        self.load_shap_explainer(shap_values_id)
        
    def load_shap_explainer(self, shap_values_id) -> None:
        
        self.explainer = shap.TreeExplainer(self.model, data=self.explainer_dataset, model_output='probability', link='logit')
#        self.explainer = shap.TreeExplainer(self.model)
        self.shap_values = self.explainer.shap_values(self.explainer_dataset)

        self.shap_values = self.shap_values[-(shap_values_id.shape[0]):, :]
        
    def generate_all_plots(self, shap_bk_info_data: pd.DataFrame, booking_ID_list: list):
        
        # drop column
        shap_bk_info_data.drop(['Account', 'Agency', 'Id', 'LastStatusDate', 'Last Status Month', 'Last Status Day', 'Decision day', 'TD %', 'D %', 'Status', 'Property'], axis=1, inplace=True)
        
        # Loop for all booking above threshold
        for i, bk_id_number in enumerate(booking_ID_list):
            
            tmp_shap_values = self.shap_values[i, :]
            tmp_shapley_tf_data = self.shapley_tf_data[self.shapley_tf_data['BK_no'] == bk_id_number]
            tmp_shap_bk_info_data = shap_bk_info_data[shap_bk_info_data['BK_no'] == bk_id_number]
            
            self.create_shap_forceplot(tmp_shap_values, tmp_shapley_tf_data, tmp_shap_bk_info_data)

    def create_shap_forceplot(self, tmp_shap_values, tmp_shapley_tf_data, tmp_shap_bk_info_data): 
        
        tmp_shapley_tf_data.drop(['BK_no'], axis=1, inplace=True)
        
        shap.force_plot(self.explainer.expected_value, tmp_shap_values, tmp_shapley_tf_data, matplotlib=True, text_rotation=15, show=False)
        
        # Add booking info table
        tmp_shap_bk_info_data.dropna(axis=1, inplace=True)
        # Convert
        tmp_shap_bk_info_data['ArrivalDate'] = tmp_shap_bk_info_data['ArrivalDate'].dt.strftime('%Y-%m-%d')
        tmp_shap_bk_info_data['DepartureDate'] = tmp_shap_bk_info_data['DepartureDate'].dt.strftime('%Y-%m-%d')
        tmp_shap_bk_info_data['BookedDate'] = tmp_shap_bk_info_data['BookedDate'].dt.strftime('%Y-%m-%d')
        # Get Booking Post As
        booking_post_as = tmp_shap_bk_info_data.loc[:, 'Post As'].values[0]

        # move Post As to front
        col = tmp_shap_bk_info_data.pop('Post As')
        tmp_shap_bk_info_data.insert(0, 'Post As', col)
    
        tmp_shap_bk_info_data = (tmp_shap_bk_info_data.T).reset_index()
        plt.table(cellText=tmp_shap_bk_info_data.values, colLabels=[' ', 'Booking Info'], loc='bottom', colLoc='center', cellLoc='center', colWidths=[0.2]*2)
        
        # Save as pdf
        booking_post_as = re.sub('[^a-zA-Z0-9 \n\.]','', booking_post_as)
        plt.savefig(self.model_path + 'shapley_forceplot\\' + booking_post_as + ' - forceplot.pdf', format='pdf', dpi=800, bbox_inches ='tight')
        plt.close()
    

# main function
def main() -> None:
    
    # Model file path
    model_path = 'I:\\10-Sales\\+Operational Reports (5Y, Restricted)\\task_scheduler\\auto_task_scheduler\\MLOps\\'
    # scl_pricing_review file path
    scl_pricing_path = 'I:\\10-Sales\\+Operational Reports (5Y, Restricted)\\task_scheduler\\auto_task_scheduler\\Revenue\\scl_pricing_review\\'

    # property list
    property_list = {'The Venetian Macao': 'VMRH', 'Conrad Macao': 'CMCC', 'Holiday Inn Macao Cotai Central': 'HICC', 'The Parisian Macao': 'PARIS', 
                     'The Londoner Macao Hotel': 'LOND'}
    # data columns
    data_col = ['RNs', 'RNs Revenue', 'F&B Revenue', 'Rental Revenue', 'AV Revenue', 'Attendance', 'ADR', 'Peak RNs']
    
    # Create property column name for empty dataframe
    column_name = ['Id']
    for key, val in property_list.items():
        for col in data_col:
            tmp = val + ' ' + col
            column_name.append(tmp)
    
    sql_data = extract_bk_materization_sql_data.Extract_SQLServer_Data()
    
    # get current date
    current_date = sql_data.now

    RoomB_data = sql_data.sql_ml_room_block_data()
    Event_data = sql_data.sql_ml_event_data()
    
    raw_booking_data = sql_data.sql_ml_bk_data()

    booking_info_data = raw_booking_data[['BK_no', 'Account: Name', 'Agency: Name', 'Post As', 'Owner']]
    booking_data = raw_booking_data.drop(['Account: Name', 'Agency: Name', 'Post As', 'Owner'], axis=1)
    
    # Preprocess booking materization data
    preprocess_materization = process_booking_materization_data.Preprocess_bk_materization_data(property_list, column_name, model_path, current_date)
    preprocess_materization.create_ml_booking_dataframe(booking_data, RoomB_data, Event_data)
    preprocess_materization.booking_ac_ag_conversion_data()
    booking_data = preprocess_materization.preprocessing_booking_data()
    
    BK_ID_no = booking_data[['BK_no']]
    
    # Prediction column name
    columns_to_standarize = ['VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue', 'VMRH Rental Revenue', 'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs',
                             'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue', 'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs', 
                             'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue', 'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs',
                             'PARIS RNs', 'PARIS RNs Revenue', 'PARIS F&B Revenue', 'PARIS Rental Revenue', 'PARIS AV Revenue', 'PARIS Attendance', 'PARIS ADR', 'PARIS Peak RNs', 
                             'LOND RNs', 'LOND RNs Revenue', 'LOND F&B Revenue', 'LOND Rental Revenue', 'LOND AV Revenue', 'LOND Attendance', 'LOND ADR', 'LOND Peak RNs',
                             'Total Account Booking', 'Account Conversion', 'Total Agency Booking', 'Agency Conversion']
    columns_to_te = ['Account: Region', 'Account: Industry', 'Agency: Region', 'Agency: Industry', 'End User Region', 
                     'End User SIC', 'Booking Type', 'Market Segment', 'Lead Source']
    column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month', 'Arrival Day', 'Departure Month',
                   'Departure Day', 'Booked Month', 'Booked Day']
    column_name_v1 = ['Last Status Month', 'Last Status Day', 'Decision day']
    
    # 
    bk_materization_predict = booking_materization_prediction_data.Booking_Materization_Prediction(booking_data, BK_ID_no, columns_to_standarize, columns_to_te, column_name, column_name_v1)
    bk_tf_data = bk_materization_predict.transforming_booking_data(model_path)
    y_pred_percent = bk_materization_predict.predicting_booking_data(model_path)

    # Percentage threshold for showing the Shapley forceplot
    threshold = 0.7
    posprocessing_bk_materization = process_booking_materization_data.Postprocessing_bk_materization_data(y_pred_percent, threshold)
    posprocessing_bk_materization.postprocessing_bk_info_data(booking_data, booking_info_data)
    booking_data_final = posprocessing_bk_materization.bk_info_joining_prediction()
    
#    prediction_to_excel(booking_data_final, model_path, scl_pricing_path)
    
    posprocessing_bk_materization.postprocessing_bk_tf_data(bk_tf_data)
    shapley_tf_data, shapley_bk_data, above_percentage = posprocessing_bk_materization.shapley_transformed_data()
    
    
    shapley = Shapley_Explainer_plot(model_path)
    shapley.load_explainer_dataset(shapley_tf_data)
    shapley.generate_all_plots(shapley_bk_data, above_percentage)
    
##################################################################################################

### For Manual run script ###

main()
        
##################################################################################################
