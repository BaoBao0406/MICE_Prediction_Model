#! python3
# process_booking_materization_data.py -

import pandas as pd
import numpy as np



class Preprocess_bk_materization_data():
    
    def __init__(self, property_list: dict, column_names: list, conversion_path: str, current_date: str):
        
        self.booking_data = None
        self.property_list = property_list
        self.column_names = column_names
        self.conversion_path = conversion_path
        self.current_date = current_date
    
    
    def create_ml_booking_dataframe(self, booking_data: pd.DataFrame, RoomB_data: pd.DataFrame, Event_data: pd.DataFrame) -> pd.DataFrame:
        # Append room block and event rawdata into property column dataframe and merge with booking dataframe
        booking_tmp_data = booking_data[['Id']]
        
        for key, val in self.property_list.items():
            
            prop_data = pd.DataFrame(columns=self.column_names)
            
            RoomB_tmp_data = RoomB_data[RoomB_data['Property'] == key]
            Event_tmp_data = Event_data[Event_data['Property'] == key]
            
            Event_tmp_data.drop('Property', axis=1, inplace=True)
            
            prop_tmp_data = pd.merge(RoomB_tmp_data, Event_tmp_data, on=['Id', 'Id'], how='outer')
            
            prop_tmp_data.rename({col: val + ' ' + col for col in prop_tmp_data.columns[~prop_tmp_data.columns.isin(['Id','Property'])]}, axis=1, inplace=True)
            
            prop_tmp_data.drop('Property', axis=1, inplace=True)
            
            prop_data.append(prop_tmp_data)
            
            booking_tmp_data = pd.merge(booking_tmp_data, prop_tmp_data, on=['Id', 'Id'], how='left')
        
        self.booking_data = pd.merge(booking_data, booking_tmp_data, on=['Id', 'Id'], how='left')
        

    def booking_ac_ag_conversion_data(self) -> pd.DataFrame:
        # Join Account and agency conversation table
        xls = pd.ExcelFile(self.conversion_path + 'documents\\' + 'account_conversion.xlsx')
        ac_conversion = pd.read_excel(xls, 'account')
        ag_conversion = pd.read_excel(xls, 'agency')
            
        ac_conversion = ac_conversion.sort_values('Year').drop_duplicates(['Account'], keep='last')
        ac_conversion = ac_conversion[['Account', 'Total Booking', 'Account Conversion']]
        ac_conversion.columns = ['Account', 'Total Account Booking', 'Account Conversion']
        
        ag_conversion = ag_conversion.sort_values('Year').drop_duplicates(['Agency'], keep='last')
        ag_conversion = ag_conversion[['Agency', 'Total Booking', 'Agency Conversion']]
        ag_conversion.columns = ['Agency', 'Total Agency Booking', 'Agency Conversion']
        
        self.booking_data = pd.merge(self.booking_data, ac_conversion, on=['Account', 'Account'], how='left')
        self.booking_data = pd.merge(self.booking_data, ag_conversion, on=['Agency', 'Agency'], how='left')


    def preprocessing_booking_data(self) -> pd.DataFrame:
    
        self.booking_data[['RSO Manager', 'Promotion']] = self.booking_data[['RSO Manager', 'Promotion']].replace(np.nan, 0)
        self.booking_data['RSO Manager'] = self.booking_data['RSO Manager'].apply(lambda x: 0 if x == 0 else 1)
        self.booking_data['Promotion'] = self.booking_data['Promotion'].apply(lambda x: 0 if x == 0 else 1)
        
        # Date calculation
        self.booking_data['ArrivalDate'] = pd.to_datetime(self.booking_data['ArrivalDate'])
        self.booking_data['Arrival Month'] = self.booking_data['ArrivalDate'].dt.month
        self.booking_data['Arrival Day'] = self.booking_data['ArrivalDate'].dt.day
        
        self.booking_data['DepartureDate'] = pd.to_datetime(self.booking_data['DepartureDate'])
        self.booking_data['Departure Month'] = self.booking_data['DepartureDate'].dt.month
        self.booking_data['Departure Day'] = self.booking_data['DepartureDate'].dt.day
        
        self.booking_data['BookedDate'] = pd.to_datetime(self.booking_data['BookedDate'])
        self.booking_data['Booked Month'] = self.booking_data['BookedDate'].dt.month
        self.booking_data['Booked Day'] = self.booking_data['BookedDate'].dt.day
        
        self.booking_data['LastStatusDate'] = self.current_date
        self.booking_data['LastStatusDate'] = pd.to_datetime(self.booking_data['LastStatusDate'])
        self.booking_data['Last Status Month'] = self.booking_data['LastStatusDate'].dt.month
        self.booking_data['Last Status Day'] = self.booking_data['LastStatusDate'].dt.day
        
        self.booking_data['Inhouse day'] = (self.booking_data['DepartureDate'] - self.booking_data['ArrivalDate']).dt.days
        self.booking_data['Lead day'] = (self.booking_data['ArrivalDate'] - self.booking_data['BookedDate']).dt.days
        self.booking_data['Decision day'] = (self.booking_data['ArrivalDate'] - self.booking_data['LastStatusDate']).dt.days
    
        return self.booking_data
    
    
    
class Postprocessing_bk_materization_data():
    
    def __init__(self, y_pred_percent: pd.DataFrame, threshold):
        # Materization percentage threshold
        self.threshold = threshold
        # prediction data
        self.y_pred_percent = y_pred_percent
        
        self.bk_tf_data = None
        
        self.booking_data = None
        
        self.above_percentage = None
    
    
    def postprocessing_bk_info_data(self, booking_data: pd.DataFrame, booking_info_data: pd.DataFrame) -> None:
        
        booking_data = pd.merge(booking_data, self.y_pred_percent, on=['BK_no', 'BK_no'], how='left')
        self.booking_data = pd.merge(booking_data, booking_info_data, on=['BK_no', 'BK_no'], how='left')
        
        
    def postprocessing_bk_tf_data(self, bk_tf_data: pd.DataFrame) -> None:
        
        self.bk_tf_data = pd.merge(bk_tf_data, self.y_pred_percent, on=['BK_no', 'BK_no'], how='left')
        
        self.above_percentage = self.y_pred_percent[self.y_pred_percent['D %'] > self.threshold]['BK_no'].tolist()
        
    
    def bk_info_joining_prediction(self):

        tmp_booking_data = self.booking_data.drop(['Account', 'Agency', 'Id'], axis=1)

        col_list = ['Owner', 'Property', 'Account: Name', 'Agency: Name', 'Post As', 'ArrivalDate', 
                    'DepartureDate', 'TD %', 'D %', 'Status', 'Account: Region', 'Account: Industry', 
                    'Agency: Region', 'Agency: Industry', 'End User Region', 'End User SIC', 
                    'Booking Type','Market Segment', 'RSO Manager', 'Promotion', 'Lead Source', 
                    'BookedDate', 'VMRH RNs', 'VMRH RNs Revenue', 'VMRH ADR', 'VMRH Peak RNs', 
                    'VMRH Attendance', 'VMRH F&B Revenue', 'VMRH AV Revenue', 'VMRH Rental Revenue', 
                    'CMCC RNs', 'CMCC RNs Revenue', 'CMCC ADR', 'CMCC Peak RNs', 'CMCC Attendance', 
                    'CMCC F&B Revenue', 'CMCC AV Revenue', 'CMCC Rental Revenue', 'HICC RNs', 
                    'HICC RNs Revenue', 'HICC ADR', 'HICC Peak RNs', 'HICC Attendance',
                    'HICC F&B Revenue', 'HICC AV Revenue', 'HICC Rental Revenue', 'PARIS RNs', 
                    'PARIS RNs Revenue', 'PARIS ADR', 'PARIS Peak RNs', 'PARIS Attendance', 
                    'PARIS F&B Revenue', 'PARIS AV Revenue', 'PARIS Rental Revenue', 'LOND RNs', 
                    'LOND RNs Revenue', 'LOND ADR', 'LOND Peak RNs', 'LOND Attendance', 'LOND F&B Revenue',
                    'LOND AV Revenue', 'LOND Rental Revenue', 'Total Account Booking', 'Account Conversion', 
                    'Total Agency Booking', 'Agency Conversion', 'Arrival Month', 'Arrival Day', 
                    'Departure Month', 'Departure Day', 'Booked Month', 'Booked Day', 'Inhouse day', 'Lead day', 
                    'BK_no']
        
        # Change datetime format
        tmp_booking_data.sort_values(by=['ArrivalDate'], inplace=True)
        tmp_booking_data['ArrivalDate'] = tmp_booking_data['ArrivalDate'].dt.strftime('%m/%d/%Y')
        tmp_booking_data['DepartureDate'] = tmp_booking_data['DepartureDate'].dt.strftime('%m/%d/%Y')
        tmp_booking_data['BookedDate'] = tmp_booking_data['BookedDate'].dt.strftime('%m/%d/%Y')
        tmp_booking_data = tmp_booking_data[col_list]
        
        return tmp_booking_data
        
        
    def shapley_transformed_data(self) -> pd.DataFrame:
        
        # using BK ID number to get transformed data
        shapley_tf_data = self.bk_tf_data[self.bk_tf_data['BK_no'].isin(self.above_percentage)]
        shapley_tf_data.sort_values(by=['BK_no'], inplace=True)
        shapley_tf_data.reset_index(drop=True, inplace=True)
        
        # using BK ID number to get booking data
        shapley_bk_data = self.booking_data[self.booking_data['BK_no'].isin(self.above_percentage)]
        shapley_bk_data.sort_values(by=['BK_no'], inplace=True)
        shapley_bk_data.reset_index(drop=True, inplace=True)
        
        return shapley_tf_data, shapley_bk_data, self.above_percentage
