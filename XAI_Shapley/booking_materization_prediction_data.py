#! python3
# booking_materization_prediction_data.py -

import pandas as pd
import pickle, re


class Booking_Materization_Prediction():
    
    def __init__(self, booking_data: pd.DataFrame, BK_ID_no: pd.DataFrame, columns_to_standarize: list, columns_to_te: list, column_name: list, column_name_v1: list):
        # raw booking data
        self.booking_data = booking_data
        # transformed dataframe
        self.data_tmp = None
        # Booking ID number
        self.BK_ID_no = BK_ID_no
        
        self.columns_to_standarize = columns_to_standarize
        self.columns_to_te = columns_to_te
        self.column_name = column_name
        self.column_name_v1 = column_name_v1
        
        self.col = self.columns_to_standarize + self.columns_to_te + self.column_name


    def transforming_booking_data(self, model_path: str) -> None:
        
        transformer = open(model_path + '\\Model\\' + 'xgb_materization_transformer.pkl', 'rb')
        transformer = pickle.load(transformer)
        
        data_tmp = pd.DataFrame(self.booking_data, columns=self.col)
        
        data_tmp = transformer.transform(data_tmp)
        data_tmp = pd.DataFrame(data_tmp, columns=self.col)
        data_tmp = data_tmp.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
        self.data_tmp = data_tmp.astype(float)
        
        bk_tf_data = pd.concat([self.BK_ID_no, self.data_tmp], axis=1)
        
        return bk_tf_data


    def predicting_booking_data(self, model_path: str) -> pd.DataFrame:
        
        model = open(model_path + '\\Model\\' + 'xgb_materization_model.pkl', 'rb')
        model = pickle.load(model)
        
        y_pred_percent = model.predict_proba(self.data_tmp)
        y_pred_percent = pd.DataFrame(y_pred_percent, columns = ['TD %', 'D %'])
        y_pred_percent.reset_index(drop=True, inplace=True)
    
        y_pred_percent = pd.concat([self.BK_ID_no, y_pred_percent], axis=1)
        
        return y_pred_percent
