import numpy as np
import pandas as pd
from datetime import datetime
import re, pickle, shutil, shap
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
import category_encoders as ce


model_path = 'H:\\Python scripts\\Machine Learning\\MICE\\'

model_name = '20230922'

model_path2 = '\\Error Analysis\\xgb\\'

# load transformer
transformer = open(model_path + model_path2 + model_name + '\\' + model_name + '_xgb_materization_transformer.pkl', 'rb')
transformer = pickle.load(transformer)


# Load rawdata
data = pd.read_excel(model_path + '\\dataset\\ml_raw_data.xlsx')


data = data.drop(['Unnamed: 0', 'Account', 'Agency', 'Id', 'BookedDate', 'ArrivalDate', 'DepartureDate', 'LastStatusDate', 'Arrival Year',
                  'Departure Year', 'Booked Year', 'Last Status Year'], axis=1)

data['Status'] = data['Status'].apply(lambda x: 1 if x == 'Definite' else 0)


replace_category_np = ['Account: Industry', 'Account: Region', 'Agency: Region', 'Agency: Industry', 'End User SIC', 'Booking Type', 'Market Segment']
data[replace_category_np] = data[replace_category_np].replace('-', np.NaN)

X = data.loc[:, data.columns != 'Status']
y = data['Status']

columns_to_standarize = ['VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue', 'VMRH Rental Revenue', 'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs',
                         'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue', 'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs', 
                         'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue', 'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs',
                         'PARIS RNs', 'PARIS RNs Revenue', 'PARIS F&B Revenue', 'PARIS Rental Revenue', 'PARIS AV Revenue', 'PARIS Attendance', 'PARIS ADR', 'PARIS Peak RNs', 
                         'LOND RNs', 'LOND RNs Revenue', 'LOND F&B Revenue', 'LOND Rental Revenue', 'LOND AV Revenue', 'LOND Attendance', 'LOND ADR', 'LOND Peak RNs',
                         'Total Account Booking', 'Account Conversion', 'Total Agency Booking', 'Agency Conversion']
columns_to_te = ['Account: Region', 'Account: Industry', 'Agency: Region', 'Agency: Industry', 'End User Region', 
                 'End User SIC', 'Booking Type', 'Market Segment', 'Lead Source']

column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Booked Month', 'Booked Day']
#column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Booked Month', 'Booked Day', 'Last Status Month', 'Last Status Day', 'Decision day']

X = X[columns_to_standarize + columns_to_te + column_name]

transformers = [('standarize', StandardScaler(), columns_to_standarize),
                ('te', ce.TargetEncoder(), columns_to_te)]

ct = ColumnTransformer(transformers, remainder='passthrough')

ct.fit(X, y)
X_te = ct.transform(X)

columns_name = columns_to_standarize + list(ct.named_transformers_['te'].get_feature_names()) + column_name
X_te = pd.DataFrame(X_te, columns=columns_name)

X_te = X_te.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

X_te.to_excel(model_path + model_path2 + model_name + '\\' + model_name + '_xai_explainer_dataset.xlsx', index=False)
