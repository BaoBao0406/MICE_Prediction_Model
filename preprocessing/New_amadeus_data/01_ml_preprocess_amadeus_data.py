#! python3
# amadeus_sql_data.py - query sql data from sql server

import pyodbc
from datetime import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta


class Date_Range:
    
    def __init__(self):
        # Set date range
        self.now = datetime.now()
        self.start_date = '2016-01-01'
        # End date
        self.end_year_arrival = self.now + relativedelta(years=5)
        self.end_date_arrival = str(self.end_year_arrival.year) + '-12-31'
    

class Extract_SQLServer_Data(Date_Range):
    
    def __init__(self):
        super().__init__()
        self.conn = self.server_connection()
        self.user = self.user_data()
        
        
    def server_connection(self) -> object:
        # Create connection
        self.conn = pyodbc.connect('Driver={SQL Server};'
                                          'Server=VOPPSCLDBN01\VOPPSCLDBI01;'
                                          'Database=SalesForce;'
                                          'Trusted_Connection=yes;')
        return self.conn
        
    def user_data(self) -> dict:
        # FDC User ID and Name list
        self.user = pd.read_sql('SELECT DISTINCT(Id), Name \
                                 FROM dbo.[User]', self.conn)
        self.user = self.user.set_index('Id')['Name'].to_dict()
        
        return self.user
    
    
    def sql_ml_bk_data(self) -> pd.DataFrame:
        # query ml booking data
        BK_ml_data = pd.read_sql("SELECT ac.Id, ag.Id, BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'yyyy-MM-dd') AS DepartureDate, BK.nihrm__Property__c, ag.nihrm__RegionName__c, ag.Industry, BK.End_User_Region__c, \
                                         BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.RSO_Manager__c, ac.nihrm__RegionName__c, ac.Industry, \
                                         BK.nihrm__BookingMarketSegmentName__c, BK.Promotion__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, BK.nihrm__BookingStatus__c, FORMAT(BK.nihrm__LastStatusDate__c, 'yyyy-MM-dd') AS LastStatusDate, BK.nihrm__LeadSourceName__c  \
                                  FROM dbo.nihrm__Booking__c AS BK \
                                     LEFT JOIN dbo.Account AS ac \
                                         ON BK.nihrm__Account__c = ac.Id \
                                     LEFT JOIN dbo.Account AS ag \
                                         ON BK.nihrm__Agency__c = ag.Id \
                                  WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                        (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.nihrm__BookingStatus__c NOT IN ('Tentative', 'Prospect'))", self.conn)
        BK_ml_data['RSO_Manager__c'].replace(self.user, inplace=True)
        BK_ml_data.columns = ['Account', 'Agency', 'Id', 'BK_no', 'ArrivalDate', 'DepartureDate', 'Property', 'Agency: Region', 'Agency: Industry', 'End User Region',
                              'End User SIC', 'Booking Type', 'RSO Manager', 'Account: Region', 'Account: Industry', 'Market Segment', 'Promotion', 'BookedDate', 'Status', 'LastStatusDate', 'Lead Source']
        
        return BK_ml_data
    
    
    def sql_ml_room_block_data(self) -> pd.DataFrame:
        # query ml room block data
        RoomB_ml_data = pd.read_sql("SELECT RoomB.nihrm__Booking__c, prop.Name, RoomB.nihrm__CurrentBlendedRoomnightsTotal__c, RoomB.nihrm__CurrentBlendedRevenueTotal__c, RoomB.nihrm__CurrentBlendedADR__c, RoomB.nihrm__PeakRoomnightsAgreed__c \
                                     FROM dbo.nihrm__BookingRoomBlock__c AS RoomB \
                                         INNER JOIN dbo.nihrm__Location__c AS prop \
                                             ON RoomB.nihrm__Location__c = prop.Id", self.conn)
        RoomB_ml_data.columns = ['Id', 'Property', 'RNs', 'RNs Revenue', 'ADR', 'Peak RNs']
        RoomB_ml_data = RoomB_ml_data[['Id', 'Property', 'RNs', 'RNs Revenue', 'ADR', 'Peak RNs']]
        RoomB_ml_data = RoomB_ml_data.groupby(['Id', 'Property']).agg({'RNs': 'sum', 'RNs Revenue': 'sum', 'ADR': 'max', 'Peak RNs': 'max'})
        RoomB_ml_data.reset_index(inplace=True)
        
        return RoomB_ml_data
    
    
    def sql_ml_event_data(self) -> pd.DataFrame:
        # query ml event data
        Event_ml_data = pd.read_sql("SELECT ET.nihrm__Booking__c, ET.nihrm__Property__c, MAX(ET.nihrm__AgreedAttendance__c), SUM(ET.nihrm__CurrentBlendedRevenue1__c), SUM(ET.nihrm__CurrentBlendedRevenue2__c), SUM(ET.nihrm__CurrentBlendedRevenue9__c), SUM(ET.nihrm__CurrentBlendedRevenue4__c), SUM(ET.nihrm__CurrentBlendedRevenue7__c) \
                                     FROM dbo.nihrm__BookingEvent__c AS ET \
                                     GROUP BY ET.nihrm__Property__c, ET.nihrm__Booking__c", self.conn)
        Event_ml_data.columns = ['Id', 'Property', 'Attendance', 'Food Revenue', 'Beverage Revenue', 'Outlet Revenue', 'AV Revenue', 'Rental Revenue']
        Event_ml_data['F&B Revenue'] = Event_ml_data['Food Revenue'] + Event_ml_data['Beverage Revenue'] + Event_ml_data['Outlet Revenue']
        Event_ml_data = Event_ml_data[['Id', 'Property', 'Attendance', 'F&B Revenue', 'AV Revenue', 'Rental Revenue']]
        
        return Event_ml_data
    

# Seperate RNs, Revenue, Peak RNs, Attendance into properties

path = 'H:\\Python scripts\\Machine Learning\\amaedus_data\\'

sql_data = Extract_SQLServer_Data()

booking_data = sql_data.sql_ml_bk_data()
RoomB_data = sql_data.sql_ml_room_block_data()
Event_data = sql_data.sql_ml_event_data()

booking_tmp_data = booking_data[['Id']]

property_list = {'The Venetian Macao': 'VMRH', 'Conrad Macao': 'CMCC', 'Holiday Inn Macao Cotai Central': 'HICC', 'The Parisian Macao': 'PARIS', 'The Londoner Macao Hotel': 'LOND'}

prop_data = pd.DataFrame()

for key, val in property_list.items():
    
    RoomB_tmp_data = RoomB_data[RoomB_data['Property'] == key]
    
    Event_tmp_data = Event_data[Event_data['Property'] == key]
    
    Event_tmp_data.drop('Property', axis=1, inplace=True)
    
    prop_tmp_data = pd.merge(RoomB_tmp_data, Event_tmp_data, on=['Id', 'Id'], how='outer')
    
    prop_tmp_data.rename({col: val + ' ' + col for col in prop_tmp_data.columns[~prop_tmp_data.columns.isin(['Id','Property'])]}, axis=1, inplace=True)
    
    prop_tmp_data.drop('Property', axis=1, inplace=True)
    
    booking_tmp_data = pd.merge(booking_tmp_data, prop_tmp_data, on=['Id', 'Id'], how='left')


booking_data = pd.merge(booking_data, booking_tmp_data, on=['Id', 'Id'], how='left')
    




# Calculate account and agency conversion

def account_conv(data_tmp, year):
    # Account conversion
    total_account = data_tmp.groupby(['Account']).agg({'Id': 'count'})
    total_account = total_account.reset_index()
    total_account.columns = ['Account', 'Total Booking']
    
    account_tmp_data = data_tmp[data_tmp['Status'] == 'Definite']
    def_account = account_tmp_data.groupby(['Account', 'Status']).agg({'Id': 'count'})
    def_account = def_account.reset_index()
    def_account.columns = ['Account', 'Status', 'Total Def']
    
    account_conversion = total_account.merge(def_account, on='Account', how='left')
    account_conversion = account_conversion.sort_values(by='Total Booking')
    
    account_conversion['Account Conversion'] = account_conversion['Total Def'] / account_conversion['Total Booking']
    account_conversion.drop('Status', axis=1, inplace=True)
    account_conversion['Year'] = year + 1
    
    return account_conversion


def agency_conv(data_tmp, year):
    # Agency conversion
    tmp_agency_data = data_tmp[data_tmp['Agency'].notnull()]
    total_agency = tmp_agency_data.groupby(['Agency']).agg({'Id': 'count'})
    total_agency = total_agency.reset_index()
    total_agency.columns = ['Agency', 'Total Booking']
    
    tmp_agency_data = tmp_agency_data[tmp_agency_data['Status'] == 'Definite']
    def_agency = tmp_agency_data.groupby(['Agency', 'Status']).agg({'Id': 'count'})
    def_agency = def_agency.reset_index()
    def_agency.columns = ['Agency', 'Status', 'Total Def']
    
    agency_conversion = total_agency.merge(def_agency, on='Agency', how='left')
    agency_conversion = agency_conversion.sort_values(by='Total Booking')
    
    agency_conversion['Agency Conversion'] = agency_conversion['Total Def'] / agency_conversion['Total Booking']
    agency_conversion.drop('Status', axis=1, inplace=True)
    agency_conversion['Year'] = year + 1
    
    return agency_conversion


years = [2016, 2017, 2018, 2019, 2020, 2021, 2022]

booking_data['ArrivalYear'] = pd.to_datetime(booking_data['ArrivalDate']).dt.year

data_added = pd.DataFrame()
account_conversion = pd.DataFrame()
agency_conversion = pd.DataFrame()

a_g_conversion = booking_data[['Account', 'Agency', 'Status', 'Id', 'ArrivalYear']]

for y in years:
    
    a_g_conversion_tmp = a_g_conversion[a_g_conversion['ArrivalYear'] == y]
    
    data_added = pd.concat([data_added, a_g_conversion_tmp])
    
    # Account conversion
    account_conversion_tmp = account_conv(data_added, y)
    account_conversion = pd.concat([account_conversion, account_conversion_tmp])
    # Agency conversion
    agency_conversion_tmp = agency_conv(data_added, y)
    agency_conversion = pd.concat([agency_conversion, agency_conversion_tmp])


account_conversion['Account_date_id'] = account_conversion['Account'].apply(str) + '&' +  account_conversion['Year'].apply(str)
agency_conversion['Agency_date_id'] = agency_conversion['Agency'].apply(str) + '&' +  agency_conversion['Year'].apply(str) 

with pd.ExcelWriter('account_conversion.xlsx') as writer:  
    account_conversion.to_excel(writer, sheet_name='account')  
    agency_conversion.to_excel(writer, sheet_name='agency')  


account_conversion = account_conversion[['Account_date_id', 'Total Booking', 'Account Conversion']]
account_conversion.columns = ['Account_date_id', 'Total Account Booking', 'Account Conversion']
agency_conversion = agency_conversion[['Agency_date_id', 'Total Booking', 'Agency Conversion']]
agency_conversion.columns = ['Agency_date_id', 'Total Agency Booking', 'Agency Conversion']


booking_data['Account_date_id'] = booking_data['Account'].apply(str) + '&' +  booking_data['ArrivalYear'].apply(str)
booking_data['Agency_date_id'] = booking_data['Agency'].apply(str) + '&' +  booking_data['ArrivalYear'].apply(str)

booking_data = booking_data.merge(account_conversion, on=['Account_date_id', 'Account_date_id'], how='left')
booking_data = booking_data.merge(agency_conversion, on=['Agency_date_id', 'Agency_date_id'], how='left')

booking_data.drop(['ArrivalYear', 'Account_date_id', 'Agency_date_id'], axis=1, inplace=True)

booking_data.to_excel(path + 'ml_amadeus_final_data.xlsx', index=False)

#booking_data.to_excel('rawdata.xlsx', index=False)
