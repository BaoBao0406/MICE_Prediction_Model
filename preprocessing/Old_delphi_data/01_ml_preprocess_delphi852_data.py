import pandas as pd


def account_conv(data_tmp, year):
    # Account conversion
    total_account = data_tmp.groupby(['Account ID']).agg({'Booking ID': 'count'})
    total_account = total_account.reset_index()
    total_account.columns = ['Account ID', 'Total Booking']
    
    account_tmp_data = data_tmp[data_tmp['Status'] == 'DEF']
    def_account = account_tmp_data.groupby(['Account ID', 'Status']).agg({'Booking ID': 'count'})
    def_account = def_account.reset_index()
    def_account.columns = ['Account ID', 'Status', 'Total Def']
    
    account_conversion = total_account.merge(def_account, on='Account ID', how='left')
    account_conversion = account_conversion.sort_values(by='Total Booking')
    
    account_conversion['Account Conversion'] = account_conversion['Total Def'] / account_conversion['Total Booking']
    account_conversion.drop('Status', axis=1, inplace=True)
    account_conversion['Year'] = year + 1
    
    return account_conversion


def agency_conv(data_tmp, year):
    # Agency conversion
    tmp_agency_data = data_tmp[data_tmp['Agency ID'].notnull()]
    total_agency = tmp_agency_data.groupby(['Agency ID']).agg({'Booking ID': 'count'})
    total_agency = total_agency.reset_index()
    total_agency.columns = ['Agency ID', 'Total Booking']
    
    tmp_agency_data = tmp_agency_data[tmp_agency_data['Status'] == 'DEF']
    def_agency = tmp_agency_data.groupby(['Agency ID', 'Status']).agg({'Booking ID': 'count'})
    def_agency = def_agency.reset_index()
    def_agency.columns = ['Agency ID', 'Status', 'Total Def']
    
    agency_conversion = total_agency.merge(def_agency, on='Agency ID', how='left')
    agency_conversion = agency_conversion.sort_values(by='Total Booking')
    
    agency_conversion['Agency Conversion'] = agency_conversion['Total Def'] / agency_conversion['Total Booking']
    agency_conversion.drop('Status', axis=1, inplace=True)
    agency_conversion['Year'] = year + 1
    
    return agency_conversion



years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016]

data = pd.DataFrame()
account_conversion = pd.DataFrame()
agency_conversion = pd.DataFrame()

for y in years:
    path = 'H:\\Python scripts\\Machine Learning\\new_delphi852_data\\dataset\\ml_delphi852_data_' + str(y) +'.xlsx'

    data_tmp = pd.read_excel(path)
    data_tmp = data_tmp[['Account ID', 'Agency ID', 'Booking ID', 'Status']]
    
    data = pd.concat([data, data_tmp])
    
    account_conversion_tmp = account_conv(data, y)
    account_conversion = pd.concat([account_conversion, account_conversion_tmp])
    
    agency_conversion_tmp = agency_conv(data, y)
    agency_conversion = pd.concat([agency_conversion, agency_conversion_tmp])


account_conversion['Account_date_id'] = account_conversion['Account ID'].apply(str) + '&' + account_conversion['Year'].apply(str)
agency_conversion['Agency_date_id'] = agency_conversion['Agency ID'].apply(str) + '&' + agency_conversion['Year'].apply(str)

with pd.ExcelWriter('account_conversion.xlsx') as writer:  
    account_conversion.to_excel(writer, sheet_name='account')  

    agency_conversion.to_excel(writer, sheet_name='agency')  
    

# Join with booking info

path = 'H:\\Python scripts\\Machine Learning\\new_delphi852_data\\dataset\\ml_delphi852_data_combined.xlsx'

all_data = pd.read_excel(path)

all_data['ArrivalYear'] = pd.to_datetime(all_data['Arrival Date']).dt.year
all_data['Account_date_id'] = all_data['Account ID'].apply(str) + '&' +  all_data['ArrivalYear'].apply(str)
all_data['Agency_date_id'] = all_data['Agency ID'].apply(str) + '&' +  all_data['ArrivalYear'].apply(str)

account_conversion = account_conversion[['Account_date_id', 'Total Booking', 'Account Conversion']]
account_conversion.columns = ['Account_date_id', 'Total Account Booking', 'Account Conversion']
agency_conversion = agency_conversion[['Agency_date_id', 'Total Booking', 'Agency Conversion']]
agency_conversion.columns = ['Agency_date_id', 'Total Agency Booking', 'Agency Conversion']

all_data = all_data.merge(account_conversion, on=['Account_date_id', 'Account_date_id'], how='left')
all_data = all_data.merge(agency_conversion, on=['Agency_date_id', 'Agency_date_id'], how='left')

all_data.drop(['ArrivalYear', 'Account_date_id', 'Agency_date_id', 'Unnamed: 0'], axis=1, inplace=True)

all_data.to_excel('H:\\Python scripts\\Machine Learning\\new_delphi852_data\\ml_delphi852_final_data.xlsx', index=False)
