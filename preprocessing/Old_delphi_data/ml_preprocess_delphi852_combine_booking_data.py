import pandas as pd

path = 'H:\\Python scripts\\Machine Learning\\new_delphi852_data\\Team Production for the year (2016).xls'

data = pd.read_excel(path)




# property column data
prop_data_tmp = data[['Booking ID', 'Property', 'Same booking', 'Agreed Guestrooms', 'Agreed Guestroom Revenue', 'Food+Bev Rev',
                      'Rental Rev', 'Resource Rev', 'Max Attendance', 'Avg Rate', 'Blocked Peak']]

prop_data_tmp.columns = ['Booking ID', 'Property', 'Same booking', 'RNs', 'RNs Revenue', 'F&B Revenue',
                      'Rental Revenue', 'AV Revenue', 'Attendance', 'ADR', 'Peak RNs']

vmrh_data = prop_data_tmp[prop_data_tmp['Property'] == 'VMRH']

vmrh_data.columns = ['Booking ID', 'Property', 'Same booking', 'VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue',
                     'VMRH Rental Revenue', 'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs']

cmcc_data = prop_data_tmp[prop_data_tmp['Property'] == 'CMCC']

cmcc_data.columns = ['Booking ID', 'Property', 'Same booking', 'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue', 
                     'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs']

hicc_data = prop_data_tmp[prop_data_tmp['Property'] == 'HICC']

hicc_data.columns = ['Booking ID', 'Property', 'Same booking', 'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue', 
                     'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs']

new_delphi_prop_data = pd.concat([vmrh_data, cmcc_data, hicc_data], ignore_index=True)


main_prop_data = new_delphi_prop_data[~(new_delphi_prop_data['Same booking'] > 0)]
main_prop_data = main_prop_data.drop(['Property', 'Same booking'], axis=1)

cross_prop_data = new_delphi_prop_data[new_delphi_prop_data['Same booking'] > 0]

cross_prop_data = cross_prop_data.groupby(['Same booking']).agg({'VMRH RNs': 'sum', 'VMRH RNs Revenue': 'sum', 'VMRH F&B Revenue': 'sum', 'VMRH Rental Revenue': 'sum', 'VMRH AV Revenue': 'sum', 'VMRH Attendance': 'max', 'VMRH ADR': 'max', 'VMRH Peak RNs': 'max',
                                                                 'CMCC RNs': 'sum', 'CMCC RNs Revenue': 'sum', 'CMCC F&B Revenue': 'sum', 'CMCC Rental Revenue': 'sum', 'CMCC AV Revenue': 'sum', 'CMCC Attendance': 'max', 'CMCC ADR': 'max', 'CMCC Peak RNs': 'max',
                                                                 'HICC RNs': 'sum', 'HICC RNs Revenue': 'sum', 'HICC F&B Revenue': 'sum', 'HICC Rental Revenue': 'sum', 'HICC AV Revenue': 'sum', 'HICC Attendance': 'max', 'HICC ADR': 'max', 'HICC Peak RNs': 'max'})

cross_prop_data = cross_prop_data.reset_index()
cross_prop_data.rename(columns={'Same booking':'Booking ID'}, inplace=True)

new_delphi_prop_data = pd.concat([main_prop_data, cross_prop_data], ignore_index=True)


#new_delphi_prop_data.to_excel('new_delphi_prop_data.xlsx')





# booking combine data
booking_info_data_tmp = data[['Account ID', 'Agency ID', 'Booking ID', 'Same booking', 'main property', 'Property', 'Account', 'Acc Region', 'Acc SICcode', 'Agency', 'Agency Region', 
                              'Agency SIC', 'Created Date', 'Status', 'Arrival Date', 'Departure Date', 'End User Region', 'End User SICCode', 'Bkg Type', 'Status Change Date', 
                              'RSM', 'Market Segment', 'Bkg Lead Source', 'Promotion']]

main_booking_info_data = booking_info_data_tmp[~(booking_info_data_tmp['Same booking'] > 0)]


cross_booking_info_data = booking_info_data_tmp[booking_info_data_tmp['Same booking'] > 0]

cross_booking_date_tmp = cross_booking_info_data[['Booking ID', 'Same booking', 'main property', 'Created Date', 'Status', 'Arrival Date', 'Departure Date', 'Status Change Date']]

cross_booking_date_tmp = cross_booking_date_tmp.groupby(['Same booking']).agg({'Created Date': 'min', 'Arrival Date': 'min', 'Departure Date': 'max', 'Status Change Date': 'min'})

cross_booking_date_tmp = cross_booking_date_tmp.reset_index()


cross_booking_info_data = cross_booking_info_data[['Same booking', 'main property', 'Account ID', 'Agency ID', 'Property', 'Account', 'Acc Region', 'Acc SICcode', 'Agency', 'Agency Region','Agency SIC', 'Status', 'End User Region', 
                                                   'End User SICCode', 'Bkg Type', 'RSM', 'Market Segment', 'Bkg Lead Source', 'Promotion']]
cross_booking_info_data = cross_booking_info_data[cross_booking_info_data['main property'] == 1]
cross_booking_info_data = cross_booking_info_data.drop(['main property'], axis=1)

cross_booking_info_data = pd.merge(cross_booking_info_data, cross_booking_date_tmp, on=['Same booking', 'Same booking'])
cross_booking_info_data.rename(columns={'Same booking':'Booking ID'}, inplace=True)

main_booking_info_data = main_booking_info_data.drop(['Same booking', 'main property'], axis=1)


new_delphi_booking_info_data = pd.concat([main_booking_info_data, cross_booking_info_data], ignore_index=True)

#new_delphi_booking_info_data.to_excel('new_delphi_booking_info_data.xlsx')




new_delphi_data = pd.merge(new_delphi_booking_info_data, new_delphi_prop_data, on=['Booking ID', 'Booking ID'])

new_delphi_data.to_excel('ml_delphi852_data_2016.xlsx')
