import pandas as pd
import numpy as np

path = 'H:\\Python scripts\\Machine Learning\\MICE\\Dataset\\'

# Delphi852 data
delphi852_data = pd.read_excel(path + 'ml_delphi852_final_data.xlsx')

delphi852_data.columns = ['Account', 'Agency', 'Id', 'AccountName', 'Account: Region', 'Account: Industry', 'AgencyName', 'Agency: Region', 'Agency: Industry', 'BookedDate',
                          'Status', 'ArrivalDate', 'DepartureDate', 'End User Region', 'End User SIC', 'Booking Type', 'LastStatusDate', 'RSO Manager',
                          'Market Segment', 'Lead Source', 'Promotion', 'VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue', 'VMRH Rental Revenue',
                          'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs', 'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue',
                          'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs', 'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue',
                          'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs', 'Total Account Booking', 'Account Conversion',
                           'Total Agency Booking', 'Agency Conversion']

delphi852_data.drop(['AccountName', 'AgencyName'], axis=1, inplace=True)

# Amadeus data
amadeus_data = pd.read_excel(path + 'ml_amadeus_final_data.xlsx')
amadeus_data.drop(['BK_no', 'Property'], axis=1, inplace=True)

# merge two dataset
BKdata = pd.concat([delphi852_data, amadeus_data])

# preprocesing and cleaning data
BKdata['Account: Region'].replace('-', np.nan, inplace=True)
BKdata['Agency: Region'] = BKdata['Agency: Region'].fillna(value='-')
BKdata['End User Region'].replace('-', np.nan, inplace=True)

BKdata['RSO Manager'] = BKdata['RSO Manager'].apply(lambda x: 0 if x is np.nan else 1)

BKdata['Promotion'].replace('-', np.nan, inplace=True)
BKdata['Promotion'] = BKdata['Promotion'].apply(lambda x: 0 if x is np.nan else 1)

STATUS = {'DEF': 'Definite', 'TURN': 'TurnedDown', 'CAN': 'Cancelled'}
BKdata['Status'] = BKdata['Status'].replace(STATUS)

BKTYPE = {'GE': 'GE Group Exhibition/Trade Show', 'EX': 'EX Expo/Exhibition', 'GQ': 'GQ Group Corp Social',
          'GR': 'GR Group Corporate Residential', 'TS': 'GE Group Exhibition/Trade Show', 'CC': 'CC Catering - Corporate',
          'GI': 'GI Group Incentive', 'GS': 'GS Group Tour Series', 'CS': 'CS Catering - Social', 'GC': 'GC Group Convention',
          'GT': 'GT Group Tour One Time', 'GN': 'GN Group Corp Non-Res (Rm Only)', 'GA': 'GA Group Ad Hoc Leisure',
          'CN': 'CN Concert', '-': np.nan, 'GRP': np.nan}
BKdata['Booking Type'] = BKdata['Booking Type'].replace(BKTYPE)
BKdata['Booking Type'].replace('-', np.nan, inplace=True)

ACSIC = {'Animal Feed': 'Agriculture/Animal related', 'Veterinary Services': 'Agriculture/Animal related', 'Wood and Lumber': 'Agriculture/Animal related', 'Agriculture': 'Agriculture/Animal related',
         'Car & Motor Vehicle Rental': 'Automotive', 'Air Transportation/Airlines': 'Aviation', 'Aviation & Aerospace': 'Aviation', 'Architecture & Design': 'Building & Construction',
         'Administration': 'Business Orgs/Services', 'Business Associations/Organizations': 'Business Orgs/Services', 'Business Services': 'Business Orgs/Services', 'Computer Software': 'Computers, Information Tech', 
         'Internet': 'Computers, Information Tech', 'Personal Service': 'Consulting Services', 'Semiconductor': 'Electronics', 'Entertainment': 'Entertainment/Recreation',
         'Tools & Instruments': 'Engineering', 'Fishing, Hunting & Trapping': 'Entertainment/Recreation', 'Museums, Art Galleries': 'Entertainment/Recreation', 'Telecommunications': 'Computers, Information Tech',
         'Photography': 'Entertainment/Recreation', 'Sporting Goods & Recreation': 'Entertainment/Recreation', 'Toys & Hobby Goods': 'Entertainment/Recreation', 'Chemical & Allied Products': 'Agriculture/Animal related', 
         'Event & Promotional Organization': 'Event Planners', 'Incentive': 'Event Planners', 'Food Service & Restaurants': 'Food & Beverage', 'Media-TV, Radio, Print, Motion Pic, Vide': 'Media',
         'Embassy & Diplomatic Services': 'Government', 'Dental Equipment & Supplies': 'Healthcare', 'Laboratory': 'Healthcare', 'Optical': 'Healthcare', 'Non-Durable Goods, Wholesale': 'Retail Sales - Other',
         'Industrial Materials/Manufacture': 'Manufacturing', 'Packaging': 'Manufacturing', 'Paper Products': 'Manufacturing', 'Textile Manufacturing': 'Manufacturing', 
         'Advertising/Marketing': 'Media', 'Publishing': 'Media', 'Direct Marketing': 'Multi Level Marketing', 'Cleaning & Maintenance': 'Nonclassifiable Establishments',
         'Alternative Energy': 'Oil, Gas, Petroleum & Energy', 'Heating & Air Conditioning': 'Oil, Gas, Petroleum & Energy', 'Utilities-Gas,Water,Light': 'Oil, Gas, Petroleum & Energy',
         'Apparel & Retail Sale': 'Retail Sales - Other', 'Social & Civic, Fraternal Org': 'Social Organisations', 'Social Svcs, Childcare': 'Social Organisations', 'Durable Goods,Wholesale': 'Retail Sales - Luxury',
         'Cruise Lines': 'Transportation', 'Distribution Services': 'Transportation', 'Express Delivery, Courier Svc., Mailings': 'Transportation', 'Import/Export': 'Transportation',
         'Logistics Company': 'Transportation', 'Motor Vehicles & Equipment': 'Transportation', 'Public Transport': 'Transportation', 'Railroad Transportation': 'Transportation', 'Media-TV,Radio,Print,Motion Pic,Video': 'Media',
         'Footwear': 'Retail Sales - Luxury', 'Furniture, Fixtures, Int. Design': 'Retail Sales - Luxury', 'Groceries & Related Products': 'Retail Sales - Luxury',  'Watches/Jewelry': 'Retail Sales - Luxury',
         'Shipping Company': 'Transportation', 'Tour Operators': 'Travel Agencies', 'Wholesale': 'Travel Agencies', 'Safety/Security': 'Security', 'Garment Industry & Manufacturing': 'Manufacturing', np.nan: '-'}
BKdata['Account: Industry'] = BKdata['Account: Industry'].replace(ACSIC)
BKdata['Agency: Industry'] = BKdata['Agency: Industry'].replace(ACSIC)
BKdata['End User SIC'] = BKdata['End User SIC'].replace(ACSIC)

BKSIC = {'Business & Professional Org':  'Business Orgs/Services', 'Cleaning/Maintenance': 'Nonclassifiable Establishments',  'Nonclassifiable Establishment': 'Nonclassifiable Establishments',
         'Durable Goods, Wholesale': 'Retail Sales - Luxury',  'Embassy & Diplomatic Svcs': 'Government', 'Event & Promotional Org': 'Event Planners',  'Express Delivery/Courier Svc/Mailings': 'Transportation',
         'Express Delivery/CourierSvc/Mailings': 'Transportation',  'Fishing, Hunting, Trapping': 'Entertainment/Recreation',  'Furniture, Fixtures, Int Design': 'Retail Sales - Luxury', 
         'Furnitures, Fixtures, Int Design': 'Retail Sales - Luxury',  'Garment Industry Mfr': 'Manufacturing',  'Industrial Materials Mfr': 'Manufacturing',  'Medical/Hospital': 'Healthcare',
         'Motor Vehicles Equipment': 'Transportation',  'Oil, Gas, Petroleum, Energy': 'Oil, Gas, Petroleum & Energy', 'Pest Control': 'Nonclassifiable Establishments',  'Retail & Department Stores':  'Retail Sales - Other',
         'Utilities-Gas, Water, Light': 'Oil, Gas, Petroleum & Energy',  'Wood & Lumber': 'Agriculture/Animal related', 'zzThird Party': 'Travel Agencies'}
BKdata['End User SIC'] = BKdata['End User SIC'].replace(BKSIC)

# Filter all internal
BKdata = BKdata[(BKdata['End User SIC'] != 'Internal Account') | (BKdata['Account: Industry'] != 'Internal Account')]
BKdata = BKdata[(BKdata['Booking Type'] != 'IN Internal') | (BKdata['Booking Type'] != 'ALT Alternative')]

BKdata['End User SIC'] = np.where(BKdata['End User SIC'] == '-', BKdata['Account: Industry'], BKdata['End User SIC'])
BKdata['Account: Industry'] = np.where(BKdata['Account: Industry'] == '-', BKdata['End User SIC'], BKdata['Account: Industry'])
BKdata['End User Region'] = np.where(BKdata['End User Region'].isna(), BKdata['Account: Region'], BKdata['End User Region'])
BKdata['Account: Region'] = np.where(BKdata['Account: Region'].isna(), BKdata['End User Region'], BKdata['Account: Region'])
BKdata['Account: Region'] = np.where(BKdata['Account: Region'].isna(), BKdata['End User Region'], BKdata['Account: Region'])


# cleaning and preprocess date
BKdata['ArrivalDate'] = pd.to_datetime(BKdata['ArrivalDate'])
BKdata['DepartureDate'] = pd.to_datetime(BKdata['DepartureDate'])
BKdata['BookedDate'] = pd.to_datetime(BKdata['BookedDate'])
BKdata['LastStatusDate'] = pd.to_datetime(BKdata['LastStatusDate'], errors='coerce')
BKdata.dropna(subset=['LastStatusDate'], inplace=True)

BKdata['LastStatusDate'] = np.where(BKdata['LastStatusDate'].isna(), BKdata['BookedDate'], BKdata['LastStatusDate'])
BKdata['Inhouse day'] = (BKdata['DepartureDate'] - BKdata['ArrivalDate']).dt.days
BKdata['Lead day'] = (BKdata['ArrivalDate'] - BKdata['BookedDate']).dt.days
BKdata['Decision day'] = (BKdata['LastStatusDate'] - BKdata['BookedDate']).dt.days

BKdata['Arrival Year'] = BKdata['ArrivalDate'].dt.year
BKdata['Arrival Month'] = BKdata['ArrivalDate'].dt.month
BKdata['Arrival Day'] = BKdata['ArrivalDate'].dt.day

BKdata['Departure Year'] = BKdata['DepartureDate'].dt.year
BKdata['Departure Month'] = BKdata['DepartureDate'].dt.month
BKdata['Departure Day'] = BKdata['DepartureDate'].dt.day

BKdata['Booked Year'] = BKdata['BookedDate'].dt.year
BKdata['Booked Month'] = BKdata['BookedDate'].dt.month
BKdata['Booked Day'] = BKdata['BookedDate'].dt.day

BKdata['Last Status Year'] = BKdata['LastStatusDate'].dt.year
BKdata['Last Status Month'] = BKdata['LastStatusDate'].dt.month
BKdata['Last Status Day'] = BKdata['LastStatusDate'].dt.day

BKdata = BKdata[BKdata['Lead day'] > 0]
BKdata = BKdata[BKdata['Decision day'] > 0]

BKdata = BKdata[~BKdata['Arrival Year'].between(2020, 2022)]



# remove tradeshow team (Booking Type: EX) for attendance and AV Revenue
tradeshow_mask = (BKdata['Booking Type'] == 'EX Expo/Exhibition')
BKdata.loc[tradeshow_mask, 'VMRH Attendance'] = ''
BKdata.loc[tradeshow_mask, 'CMCC Attendance'] = ''
BKdata.loc[tradeshow_mask, 'HIMCC Attendance'] = ''
BKdata.loc[tradeshow_mask, 'PARIS Attendance'] = ''
BKdata.loc[tradeshow_mask, 'LOND Attendance'] = ''

BKdata.loc[tradeshow_mask, 'VMRH AV Revenue'] = ''
BKdata.loc[tradeshow_mask, 'CMCC AV Revenue'] = ''
BKdata.loc[tradeshow_mask, 'HIMCC AV Revenue'] = ''
BKdata.loc[tradeshow_mask, 'PARIS AV Revenue'] = ''
BKdata.loc[tradeshow_mask, 'LOND AV Revenue'] = ''

#TODO: all property ADR
#TODO: include weekday and weekend

BKdata.to_excel('ml_raw_data_v1.xlsx')
