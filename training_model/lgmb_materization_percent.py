import numpy as np
import pandas as pd
import math, re, pickle, csv
from scipy import stats

from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
import category_encoders as ce
from sklearn.model_selection import train_test_split

import lightgbm as lgbm
from lightgbm import LGBMClassifier
from sklearn.pipeline import Pipeline
from hyperopt import fmin, tpe, hp, anneal, Trials
from hyperopt import STATUS_OK
from hyperopt.pyll import scope
from sklearn.metrics import precision_recall_fscore_support as score


file = 'H:\\Python scripts\\Machine Learning\\MICE\\Dataset\\ml_raw_data_v1.xlsx'
data = pd.read_excel(file)

data = data.drop(['Unnamed: 0', 'Account', 'Agency', 'Id', 'BookedDate', 'ArrivalDate', 'DepartureDate', 'LastStatusDate', 'Arrival Year',
                  'Departure Year', 'Booked Year', 'Last Status Year', 'Last Status Month', 'Last Status Day', 'Decision day'], axis=1)

data['Status'] = data['Status'].apply(lambda x: 1 if x == 'Definite' else 0)


replace_category_np = ['Account: Industry', 'Account: Region', 'Agency: Region', 'Agency: Industry', 'End User SIC', 'Booking Type', 'Market Segment']
data[replace_category_np] = data[replace_category_np].replace('-', np.NaN)

X = data.loc[:, data.columns != 'Status']
y = data['Status']

columns_to_standarize = ['VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue', 'VMRH Rental Revenue', 'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs',
                         'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue', 'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs', 
                         'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue', 'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs',
                         'PARIS RNs', 'PARIS RNs Revenue', 'PARIS F&B Revenue', 'PARIS Rental Revenue', 'PARIS AV Revenue', 'PARIS Attendance', 'PARIS ADR', 'PARIS Peak RNs', 
                         'LOND RNs', 'LOND RNs Revenue', 'LOND F&B Revenue', 'LOND Rental Revenue', 'LOND AV Revenue', 'LOND Attendance', 'LOND ADR', 'LOND Peak RNs',
                         'Total Account Booking', 'Account Conversion', 'Total Agency Booking', 'Agency Conversion']
columns_to_te = ['Account: Region', 'Account: Industry', 'Agency: Region', 'Agency: Industry', 'End User Region', 
                 'End User SIC', 'Booking Type', 'Market Segment', 'Lead Source']

'''
# Option 1 - using sin&cos formula for date variables
X['Arrival Month_sin'] = np.sin(2 * np.pi * X['Arrival Month']/12)
X['Arrival Month_cos'] = np.cos(2 * np.pi * X['Arrival Month']/12)
X['Arrival Day_sin'] = np.sin(2 * np.pi * X['Arrival Day']/12)
X['Arrival Day_cos'] = np.cos(2 * np.pi * X['Arrival Day']/12)
X['Departure Month_sin'] = np.sin(2 * np.pi * X['Departure Month']/12)
X['Departure Month_cos'] = np.cos(2 * np.pi * X['Departure Month']/12)
X['Departure Day_sin'] = np.sin(2 * np.pi * X['Departure Day']/12)
X['Departure Day_cos'] = np.cos(2 * np.pi * X['Departure Day']/12)
X['Booked Month_sin'] = np.sin(2 * np.pi * X['Booked Month']/12)
X['Booked Month_cos'] = np.cos(2 * np.pi * X['Booked Month']/12)
X['Booked Day_sin'] = np.sin(2 * np.pi * X['Booked Day']/12)
X['Booked Day_cos'] = np.cos(2 * np.pi * X['Booked Day']/12)

X = X.drop(['Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Booked Month', 'Booked Day'], axis=1)

column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month_sin', 'Arrival Month_cos', 'Arrival Day_sin', 'Arrival Day_cos','Departure Month_sin',
               'Departure Month_cos', 'Departure Day_sin', 'Departure Day_cos', 'Booked Month_sin', 'Booked Month_cos', 'Booked Day_sin', 'Booked Day_cos']
'''

# Option 2 - use original numerical value
#column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Booked Month', 'Booked Day']
column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Booked Month', 'Booked Day']

X = X[columns_to_standarize + columns_to_te + column_name]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)

transformers = [('standarize', StandardScaler(), columns_to_standarize),
                ('te', ce.TargetEncoder(), columns_to_te)]

ct = ColumnTransformer(transformers, remainder='passthrough')

ct.fit(X_train, y_train)
X_train_te = ct.transform(X_train)
X_test_te = ct.transform(X_test)

columns_name = columns_to_standarize + list(ct.named_transformers_['te'].get_feature_names()) + column_name


X_train_te = pd.DataFrame(X_train_te, columns=columns_name)
X_test_te = pd.DataFrame(X_test_te, columns=columns_name)

# remove unnecessary symbols in columns name
X_train_te = X_train_te.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

N_FOLDS = 10

count = 0

lgb_train_dateset = lgbm.Dataset(X_train_te, label=y_train)

def objective(params, n_folds = N_FOLDS):
    global count
    
    params = {
              'num_boost_round': int(params['num_boost_round']), 
              'max_depth': int(params['max_depth']), 
              'learning_rate': params['learning_rate'],
              'num_leaves': int(params['num_leaves']),
              'boosting_type': params['boosting_type'],
              'colsample_bytree': params['colsample_bytree'],
              'min_data_in_leaf': int(params['min_data_in_leaf']),
              'tree_learner': params['tree_learner'],
              'subsample': params['subsample'],
              'lambda_l1': params['lambda_l1'],
              'lambda_l2': params['lambda_l2'],
              'feature_pre_filter': False,
              'bagging_freq': int(params['bagging_freq']),
#              'bagging_fraction': params['bagging_fraction']
             }
    
    cv_results = lgbm.cv(params, lgb_train_dateset, nfold = n_folds, metrics = 'auc', stratified=True, eval_train_metric=True)
    best_score = max(cv_results['valid auc-mean'])
    loss = 1 - best_score
    
    max_index = np.argmax(cv_results['valid auc-mean'])
    best_train_score = cv_results['train auc-mean'][max_index]
    best_train_score = 1 - best_train_score
    count += 1
    
    return {'loss': loss, 'params': params, 'status': STATUS_OK,
            'score': {'count': [count], 'train auc-mean': [best_train_score], 'valid auc-mean': [loss]}}
    
space={
       'num_boost_round': scope.int(hp.quniform('num_boost_round', 200, 1000, 10)),
       'max_depth' : hp.quniform('max_depth', 2, 15, 1),
       'learning_rate': hp.loguniform('learning_rate', np.log(0.005), np.log(0.5)),
       'num_leaves': scope.int(hp.quniform('num_leaves', 20, 3000, 10)),
       'boosting_type': hp.choice('boosting_type', ['gbdt', 'dart', 'rf']),
       'colsample_bytree': hp.uniform('colsample_bytree', 0.6, 0.9),
       'min_data_in_leaf': scope.int(hp.quniform('min_data_in_leaf', 100, 1000, 10)),
       'tree_learner': hp.choice('tree_learner', ['serial', 'feature', 'data', 'voting']),
       'subsample': hp.uniform('subsample', 0.5, 0.9),
       'lambda_l1': hp.quniform('lambda_l1', 1.0, 100, 0.5),
       'lambda_l2': hp.quniform('lambda_l2', 1.0, 100, 0.5),
       'bagging_freq': hp.quniform('bagging_freq', 1, 10, 1),
#       'bagging_fraction': hp.uniform('bagging_fraction', 0.5, 0.9),
       'feature_pre_filter': False,
       'early_stopping_round': 200,
      }

trials = Trials()

# number of iternations (max_evals)
num_iterations = 1000

best = fmin(fn=objective,
            space=space,
            algo=tpe.suggest,
            max_evals=num_iterations,
            trials=trials)

file_path = 'H:\\Python scripts\\Machine Learning\\MICE\\Model\\'

cv_results = pd.DataFrame()
max_evals = num_iterations

for i in range(max_evals):
    tmp = pd.DataFrame(trials.trials[i]['result']['score'])
    cv_results = cv_results.append(tmp)
score_filename = file_path + 'lgb_materization_model_iteration_score.csv'
cv_results.to_csv(score_filename)

model_name = 'lgb_materization_model'

metadata_filename = file_path + model_name + '_parameter.txt'
with open(metadata_filename, 'w') as file:
    file.write('Model Name: \n' + model_name + '\n\n')
    file.write('Best Parameter: \n' + str(best))

X_test_te = X_test_te.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

booster_type_name = 'gbdt' if best['boosting_type'] == 0 else 'dart' if best['boosting_type'] == 1 else 'rf'

tree_learner_name = 'serial' if best['tree_learner'] == 0 else 'feature' if best['tree_learner'] == 1 else 'data' if best['tree_learner'] == 2 else 'voting'

model = LGBMClassifier(num_boost_round=int(best['num_boost_round']), max_depth=int(best['max_depth']), boosting_type=booster_type_name, learning_rate=float(best['learning_rate']), 
                       num_leaves=int(best['num_leaves']), colsample_bytree=float(best['colsample_bytree']), min_data_in_leaf=int(best['min_data_in_leaf']), bagging_freq=int(best['bagging_freq']), 
                       subsample=float(best['subsample']), lambda_l2=float(best['lambda_l2']), lambda_l1=float(best['lambda_l1']), tree_learner=tree_learner_name)

model.fit(X_train_te, y_train)
y_pred_te = model.predict(X_test_te)

precision, recall, fscore, _ = score(y_test, y_pred_te)
model_score = pd.DataFrame([precision, recall, fscore], index=['precision', 'recall', 'fscore'], columns=['TD', 'D'])


metadata_filename = file_path + model_name + '_metadata.txt'
with open(metadata_filename, 'w') as file:
    file.write('Model Name: \n' + model_name + '\n\n')
    file.write('Overall Model Score: \n')
    model_score_string = model_score.to_string(header=True, index=True)
    file.write(model_score_string + '\n\n')
    file.write('Best Parameter: \n' + str(best))
    

# Save ML model
pkl_filename = file_path + 'lgb_materization_model.pkl'
with open(pkl_filename, 'wb') as file:
    pickle.dump(model, file)

# Save Transformer
tf_filename = file_path + 'lgb_materization_transformer.pkl'
with open(tf_filename, 'wb') as file:
    pickle.dump(ct, file)
