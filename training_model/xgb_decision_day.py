import numpy as np
import pandas as pd
import math, re, pickle, csv
from scipy import stats

from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
import category_encoders as ce
from sklearn.model_selection import train_test_split

import xgboost as xgb
from xgboost import XGBRegressor
from sklearn.pipeline import Pipeline
from hyperopt import fmin, tpe, hp, anneal, Trials
from hyperopt import STATUS_OK
from hyperopt.pyll import scope
from sklearn.metrics import mean_absolute_error, mean_squared_error


file = 'H:\\Python scripts\\Machine Learning\\ml_raw_data.xlsx'
data = pd.read_excel(file)

data = data.drop(['Unnamed: 0', 'Account', 'Agency', 'Id', 'BookedDate', 'ArrivalDate', 'DepartureDate', 'LastStatusDate', 'Arrival Year',
                  'Departure Year', 'Booked Year', 'Last Status Year', 'Last Status Month', 'Last Status Day', 'Status'], axis=1)

replace_category_np = ['Account: Industry', 'Account: Region', 'Agency: Region', 'Agency: Industry', 'End User SIC', 'Booking Type', 'Market Segment']
data[replace_category_np] = data[replace_category_np].replace('-', np.NaN)

X = data.loc[:, data.columns != 'Decision day']
y = data['Decision day']

X['Arrival Month_sin'] = np.sin(2 * np.pi * X['Arrival Month']/12)
X['Arrival Day_sin'] = np.sin(2 * np.pi * X['Arrival Day']/12)
X['Departure Month_sin'] = np.sin(2 * np.pi * X['Departure Month']/12)
X['Departure Day_sin'] = np.sin(2 * np.pi * X['Departure Day']/12)
X['Booked Month_sin'] = np.sin(2 * np.pi * X['Booked Month']/12)
X['Booked Day_sin'] = np.sin(2 * np.pi * X['Booked Day']/12)

X = X.drop(['Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Booked Month', 'Booked Day'], axis=1)

columns_to_standarize = ['VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue', 'VMRH Rental Revenue', 'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs',
                         'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue', 'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs', 
                         'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue', 'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs',
                         'PARIS RNs', 'PARIS RNs Revenue', 'PARIS F&B Revenue', 'PARIS Rental Revenue', 'PARIS AV Revenue', 'PARIS Attendance', 'PARIS ADR', 'PARIS Peak RNs', 
                         'LOND RNs', 'LOND RNs Revenue', 'LOND F&B Revenue', 'LOND Rental Revenue', 'LOND AV Revenue', 'LOND Attendance', 'LOND ADR', 'LOND Peak RNs',
                         'Total Account Booking', 'Account Conversion', 'Total Agency Booking', 'Agency Conversion']
columns_to_te = ['Account: Region', 'Account: Industry', 'Agency: Region', 'Agency: Industry', 'End User Region', 
                 'End User SIC', 'Booking Type', 'Market Segment', 'Lead Source']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)

transformers = [('standarize', StandardScaler(), columns_to_standarize),
                ('te', ce.TargetEncoder(), columns_to_te)]

ct = ColumnTransformer(transformers, remainder='passthrough')

ct.fit(X_train, y_train)
X_train_te = ct.transform(X_train)
X_test_te = ct.transform(X_test)

column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month_sin', 'Arrival Day_sin', 'Departure Month_sin',
               'Departure Day_sin', 'Booked Month_sin', 'Booked Day_sin']
columns_name = columns_to_standarize + list(ct.named_transformers_['te'].get_feature_names()) + column_name

X_train_te = pd.DataFrame(X_train_te, columns=columns_name)
X_test_te = pd.DataFrame(X_test_te, columns=columns_name)

# remove unnecessary symbols in columns name
X_train_te = X_train_te.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))


N_FOLDS = 10

count = 0

xgb_train_dateset = xgb.DMatrix(X_train_te, label=y_train)

def objective(params, n_folds = N_FOLDS):
    global count
    
    params = {
              'num_boost_round': int(params['num_boost_round']), 
              'max_depth': int(params['max_depth']), 
              'learning_rate': params['learning_rate'],
              'booster': params['booster'],
              'colsample_bytree': params['colsample_bytree'],
              'colsample_bylevel': params['colsample_bylevel'],
              'subsample': params['subsample'],
              'min_child_weight': int(params['min_child_weight']),
              'reg_lambda': params['reg_lambda'],
              'reg_alpha': params['reg_alpha'],
             }
    
    cv_results = xgb.cv(params, xgb_train_dateset, nfold = n_folds, metrics = 'rmse', stratified=True)
    loss = min(cv_results['test-rmse-mean'])
    
    max_index = cv_results['test-rmse-mean'].idxmax()
    best_train_score = cv_results.loc[max_index, 'train-rmse-mean']
    count += 1
    
    return {'loss': loss, 'params': params, 'status': STATUS_OK,
            'score': {'count': [count], 'train-rmse-mean': [best_train_score], 'test-rmse-mean': [loss]}}
    
space={
       'num_boost_round': hp.quniform('num_boost_round', 200, 1000, 1),
       'max_depth' : hp.quniform('max_depth', 2, 15, 1),
       'learning_rate': hp.loguniform('learning_rate', np.log(0.005), np.log(0.5)),
       'booster': hp.choice('booster', ['gblinear', 'dart', 'gbtree']),
       'colsample_bytree': hp.uniform('colsample_bytree', 0.4, 0.8),
       'colsample_bylevel': hp.uniform('colsample_bylevel', 0.4, 0.8),
       'subsample': hp.uniform('subsample', 0.5, 0.9),
       'min_child_weight': hp.quniform('min_child_weight', 10, 200, 1),
       'reg_lambda': hp.quniform('reg_lambda', 1, 10, 0.2),
       'reg_alpha': hp.quniform('reg_alpha', 1, 10, 0.2),
      }

trials = Trials()

best = fmin(fn=objective,
            space=space,
            algo=tpe.suggest,
            max_evals=1000,
            trials=trials)

file_path = 'H:\\Python scripts\\Machine Learning\\Model\\'

cv_results = pd.DataFrame()
max_evals = 1000

for i in range(max_evals):
    tmp = pd.DataFrame(trials.trials[i]['result']['score'])
    cv_results = cv_results.append(tmp)
score_filename = file_path + 'xgb_decision_day_model_iteration_score.csv'
cv_results.to_csv(score_filename)

model_name = 'xgb_decision_day_model'

metadata_filename = file_path + model_name + '_parameter.txt'
with open(metadata_filename, 'w') as file:
    file.write('Model Name: \n' + model_name + '\n\n')
    file.write('Best Parameter: \n' + str(best))

X_test_te = X_test_te.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))

booster_type_name = 'gblinear' if best['booster'] == 0 else 'dart' if best['booster'] == 1 else 'gbtree'
    
model = XGBRegressor(max_depth=int(best['max_depth']), booster=booster_type_name, learning_rate=best['learning_rate'], reg_lambda=float(best['reg_lambda']), 
                     colsample_bylevel=float(best['colsample_bylevel']), colsample_bytree=float(best['colsample_bytree']), min_child_weight=int(best['min_child_weight']), 
                     subsample=float(best['subsample']), num_boost_round=int(best['num_boost_round']), reg_alpha=float(best['reg_alpha']))

model.fit(X_train_te, y=y_train)
y_pred_te = model.predict(X_test_te)


mse = (mean_squared_error(y_test, y_pred_te))
mae = (mean_absolute_error(y_test, y_pred_te))
rmse = math.sqrt(mean_squared_error(y_test, y_pred_te))
model_score = pd.DataFrame([mse, mae, rmse], index=['MSE', 'MAE', 'RMSE'], columns=['score'])


metadata_filename = file_path + model_name + '_metadata.txt'
with open(metadata_filename, 'w') as file:
    file.write('Model Name: \n' + model_name + '\n\n')
    file.write('Overall Model Score: \n')
    model_score_string = model_score.to_string(header=True, index=True)
    file.write(model_score_string + '\n\n')
    file.write('Best Parameter: \n' + str(best))
    
# Save ML model
pkl_filename = file_path + 'xgb_decision_day_model.pkl'
with open(pkl_filename, 'wb') as file:
    pickle.dump(model, file)

# Save Transformer
tf_filename = file_path + 'xgb_decision_day_transformer.pkl'
with open(tf_filename, 'wb') as file:
    pickle.dump(ct, file)
