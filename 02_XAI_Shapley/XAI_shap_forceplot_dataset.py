import pyodbc, re, pickle, shap
from datetime import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta
import numpy as np
import matplotlib.pyplot as plt


class Date_Range:
    
    def __init__(self):
        # Set date range
        self.now = datetime.now()
        

class Extract_SQLServer_Data(Date_Range):
    
    def __init__(self):
        super().__init__()
        self.conn = self.server_connection()
        self.BK_ID = None
        self.BK_ID_no = None
        
    def server_connection(self) -> object:
        # Create connection
        self.conn = pyodbc.connect('Driver={SQL Server};'
                                          'Server=VOPPSCLDBN01\VOPPSCLDBI01;'
                                          'Database=SalesForce;'
                                          'Trusted_Connection=yes;')
        return self.conn
        
    
    def sql_ml_bk_data_id_no(self, BK_ID_no) -> pd.DataFrame:
        # query ml booking data
        BK_ml_data = pd.read_sql("SELECT ac.Id, ag.Id, BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'yyyy-MM-dd') AS DepartureDate, BK.nihrm__Property__c, ag.nihrm__RegionName__c, ag.Industry, BK.End_User_Region__c, \
                                         BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.RSO_Manager__c, ac.nihrm__RegionName__c, ac.Industry, BK.nihrm__BookingMarketSegmentName__c, BK.Promotion__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, BK.nihrm__BookingStatus__c, \
                                         BK.nihrm__LeadSourceName__c, ac.Name, ag.Name, BK.Name, BK.OwnerId \
                                  FROM dbo.nihrm__Booking__c AS BK \
                                     LEFT JOIN dbo.Account AS ac \
                                         ON BK.nihrm__Account__c = ac.Id \
                                     LEFT JOIN dbo.Account AS ag \
                                         ON BK.nihrm__Agency__c = ag.Id \
                                  WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                        (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.Booking_ID_Number__c = " + BK_ID_no + ")", self.conn)
        BK_ml_data.columns = ['Account', 'Agency', 'Id', 'BK_no', 'ArrivalDate', 'DepartureDate', 'Property', 'Agency: Region', 'Agency: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'RSO Manager', 'Account: Region', 
                              'Account: Industry', 'Market Segment', 'Promotion', 'BookedDate', 'Status', 'Lead Source', 'Account: Name', 'Agency: Name', 'Post As', 'Owner']
        self.BK_ID = str(BK_ml_data.iloc[0]['Id'])
        
        return BK_ml_data, BK_ID_no
    
    
    def sql_ml_bk_data_id(self, BK_ID) -> pd.DataFrame:
        # query ml booking data
        BK_ml_data = pd.read_sql("SELECT ac.Id, ag.Id, BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'yyyy-MM-dd') AS DepartureDate, BK.nihrm__Property__c, ag.nihrm__RegionName__c, ag.Industry, BK.End_User_Region__c, \
                                         BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.RSO_Manager__c, ac.nihrm__RegionName__c, ac.Industry, BK.nihrm__BookingMarketSegmentName__c, BK.Promotion__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, BK.nihrm__BookingStatus__c, \
                                         BK.nihrm__LeadSourceName__c, ac.Name, ag.Name, BK.Name, BK.OwnerId \
                                  FROM dbo.nihrm__Booking__c AS BK \
                                     LEFT JOIN dbo.Account AS ac \
                                         ON BK.nihrm__Account__c = ac.Id \
                                     LEFT JOIN dbo.Account AS ag \
                                         ON BK.nihrm__Agency__c = ag.Id \
                                  WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                        (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.Id = '" + BK_ID + "')", self.conn)
        BK_ml_data.columns = ['Account', 'Agency', 'Id', 'BK_no', 'ArrivalDate', 'DepartureDate', 'Property', 'Agency: Region', 'Agency: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'RSO Manager', 'Account: Region', 
                              'Account: Industry', 'Market Segment', 'Promotion', 'BookedDate', 'Status', 'Lead Source', 'Account: Name', 'Agency: Name', 'Post As', 'Owner']
        self.BK_ID = str(BK_ml_data.iloc[0]['Id'])
        self.BK_ID_no = str(BK_ml_data.iloc[0]['BK_no'])
        
        return BK_ml_data, self.BK_ID_no
    
    
    def sql_ml_room_block_data(self) -> pd.DataFrame:
        # query ml room block data
        RoomB_ml_data = pd.read_sql("SELECT RoomB.nihrm__Booking__c, prop.Name, RoomB.nihrm__CurrentBlendedRoomnightsTotal__c, RoomB.nihrm__CurrentBlendedRevenueTotal__c, RoomB.nihrm__CurrentBlendedADR__c, RoomB.nihrm__PeakRoomnightsAgreed__c \
                                     FROM dbo.nihrm__BookingRoomBlock__c AS RoomB \
                                         INNER JOIN dbo.nihrm__Location__c AS prop \
                                             ON RoomB.nihrm__Location__c = prop.Id \
                                     WHERE (RoomB.nihrm__Booking__c = '" + self.BK_ID + "') AND \
                                         (prop.Name NOT IN ('Sands Macao Hotel', 'Sheraton Grand Macao', 'The St. Regis Macao', 'Four Seasons Hotel Macao Cotai Strip'))", self.conn)
        RoomB_ml_data.columns = ['Id', 'Property', 'RNs', 'RNs Revenue', 'ADR', 'Peak RNs']
        RoomB_ml_data = RoomB_ml_data[['Id', 'Property', 'RNs', 'RNs Revenue', 'ADR', 'Peak RNs']]
        RoomB_ml_data = RoomB_ml_data.groupby(['Id', 'Property']).agg({'RNs': 'sum', 'RNs Revenue': 'sum', 'ADR': 'max', 'Peak RNs': 'max'})
        RoomB_ml_data.reset_index(inplace=True)
        
        return RoomB_ml_data
    
    
    def sql_ml_event_data(self) -> pd.DataFrame:
        # query ml event data
        Event_ml_data = pd.read_sql("SELECT ET.nihrm__Booking__c, ET.nihrm__Property__c, MAX(ET.nihrm__AgreedAttendance__c), SUM(ET.nihrm__CurrentBlendedRevenue1__c), SUM(ET.nihrm__CurrentBlendedRevenue2__c), SUM(ET.nihrm__CurrentBlendedRevenue9__c), SUM(ET.nihrm__CurrentBlendedRevenue4__c), SUM(ET.nihrm__CurrentBlendedRevenue7__c) \
                                     FROM dbo.nihrm__BookingEvent__c AS ET \
                                     WHERE (ET.nihrm__Booking__c = '" + self.BK_ID + "') AND \
                                         (ET.nihrm__Property__c NOT IN ('Sands Macao Hotel', 'Sheraton Grand Macao', 'The St. Regis Macao', 'Four Seasons Hotel Macao Cotai Strip')) \
                                     GROUP BY ET.nihrm__Property__c, ET.nihrm__Booking__c", self.conn)
        Event_ml_data.columns = ['Id', 'Property', 'Attendance', 'Food Revenue', 'Beverage Revenue', 'Outlet Revenue', 'AV Revenue', 'Rental Revenue']
        Event_ml_data['F&B Revenue'] = Event_ml_data['Food Revenue'] + Event_ml_data['Beverage Revenue'] + Event_ml_data['Outlet Revenue']
        Event_ml_data = Event_ml_data[['Id', 'Property', 'Attendance', 'F&B Revenue', 'AV Revenue', 'Rental Revenue']]
        
        return Event_ml_data


def create_ml_booking_dataframe(booking_data: pd.DataFrame, RoomB_data: pd.DataFrame, Event_data: pd.DataFrame, property_list: dict, columns: list) -> pd.DataFrame:
    # Append room block and event rawdata into property column dataframe and merge with booking dataframe
    booking_tmp_data = booking_data[['Id']]
    
    for key, val in property_list.items():
        
        prop_data = pd.DataFrame(columns=columns)
        
        if RoomB_data.empty:
            RoomB_tmp_data = RoomB_data
            RoomB_tmp_data.rename(columns={'index':'Id'}, inplace=True)
            RoomB_tmp_data['Property'] = ''
        else:
            RoomB_tmp_data = RoomB_data[RoomB_data['Property'] == key]
        
        if Event_data.empty:
            Event_tmp_data = Event_data
            Event_tmp_data.rename(columns={'index':'Id'}, inplace=True)
            Event_tmp_data['Property'] = ''
        else:
            Event_tmp_data = Event_data[Event_data['Property'] == key]
        
        Event_tmp_data.drop('Property', axis=1, inplace=True)
        
        prop_tmp_data = pd.merge(RoomB_tmp_data, Event_tmp_data, on=['Id', 'Id'], how='outer')
        
        prop_tmp_data.rename({col: val + ' ' + col for col in prop_tmp_data.columns[~prop_tmp_data.columns.isin(['Id','Property'])]}, axis=1, inplace=True)
        
        prop_tmp_data.drop('Property', axis=1, inplace=True)
        
        prop_data.append(prop_tmp_data)
        
        booking_tmp_data = pd.merge(booking_tmp_data, prop_tmp_data, on=['Id', 'Id'], how='left')
    
    booking_data = pd.merge(booking_data, booking_tmp_data, on=['Id', 'Id'], how='left')
    
    return booking_data


def booking_ac_ag_conversion_data(booking_data: pd.DataFrame, path: str) -> pd.DataFrame:
    # Join Account and agency conversation table
    xls = pd.ExcelFile(path + 'documents\\' + 'account_conversion.xlsx')
    ac_conversion = pd.read_excel(xls, 'account')
    ag_conversion = pd.read_excel(xls, 'agency')
    
#    ac_conversion.drop('Unnamed: 0', axis=1, inplace=True)
#    ag_conversion.drop('Unnamed: 0', axis=1, inplace=True)
    
    ac_conversion = ac_conversion.sort_values('Year').drop_duplicates(['Account'], keep='last')
    ac_conversion = ac_conversion[['Account', 'Total Booking', 'Account Conversion']]
    ac_conversion.columns = ['Account', 'Total Account Booking', 'Account Conversion']
    
    ag_conversion = ag_conversion.sort_values('Year').drop_duplicates(['Agency'], keep='last')
    ag_conversion = ag_conversion[['Agency', 'Total Booking', 'Agency Conversion']]
    ag_conversion.columns = ['Agency', 'Total Agency Booking', 'Agency Conversion']
    
    booking_data = pd.merge(booking_data, ac_conversion, on=['Account', 'Account'], how='left')
    booking_data = pd.merge(booking_data, ag_conversion, on=['Agency', 'Agency'], how='left')
    
    return booking_data


def preprocessing_booking_data(booking_data: pd.DataFrame, current_date: str) -> pd.DataFrame:

    booking_data[['RSO Manager', 'Promotion']] = booking_data[['RSO Manager', 'Promotion']].replace(np.nan, 0)
    booking_data['RSO Manager'] = booking_data['RSO Manager'].apply(lambda x: 0 if x == 0 else 1)
    booking_data['Promotion'] = booking_data['Promotion'].apply(lambda x: 0 if x == 0 else 1)
    
    # Date calculation
    booking_data['ArrivalDate'] = pd.to_datetime(booking_data['ArrivalDate'])
    booking_data['Arrival Month'] = booking_data['ArrivalDate'].dt.month
    booking_data['Arrival Day'] = booking_data['ArrivalDate'].dt.day
    
    booking_data['DepartureDate'] = pd.to_datetime(booking_data['DepartureDate'])
    booking_data['Departure Month'] = booking_data['DepartureDate'].dt.month
    booking_data['Departure Day'] = booking_data['DepartureDate'].dt.day
    
    booking_data['BookedDate'] = pd.to_datetime(booking_data['BookedDate'])
    booking_data['Booked Month'] = booking_data['BookedDate'].dt.month
    booking_data['Booked Day'] = booking_data['BookedDate'].dt.day
    
    booking_data['LastStatusDate'] = current_date
    booking_data['LastStatusDate'] = pd.to_datetime(booking_data['LastStatusDate'])
    booking_data['Last Status Month'] = booking_data['LastStatusDate'].dt.month
    booking_data['Last Status Day'] = booking_data['LastStatusDate'].dt.day
    
    booking_data['Inhouse day'] = (booking_data['DepartureDate'] - booking_data['ArrivalDate']).dt.days
    booking_data['Lead day'] = (booking_data['ArrivalDate'] - booking_data['BookedDate']).dt.days
    booking_data['Decision day'] = (booking_data['ArrivalDate'] - booking_data['LastStatusDate']).dt.days

    return booking_data


def prediction_columns_name():
    
    columns_to_standarize = ['VMRH RNs', 'VMRH RNs Revenue', 'VMRH F&B Revenue', 'VMRH Rental Revenue', 'VMRH AV Revenue', 'VMRH Attendance', 'VMRH ADR', 'VMRH Peak RNs',
                             'CMCC RNs', 'CMCC RNs Revenue', 'CMCC F&B Revenue', 'CMCC Rental Revenue', 'CMCC AV Revenue', 'CMCC Attendance', 'CMCC ADR', 'CMCC Peak RNs', 
                             'HICC RNs', 'HICC RNs Revenue', 'HICC F&B Revenue', 'HICC Rental Revenue', 'HICC AV Revenue', 'HICC Attendance', 'HICC ADR', 'HICC Peak RNs',
                             'PARIS RNs', 'PARIS RNs Revenue', 'PARIS F&B Revenue', 'PARIS Rental Revenue', 'PARIS AV Revenue', 'PARIS Attendance', 'PARIS ADR', 'PARIS Peak RNs', 
                             'LOND RNs', 'LOND RNs Revenue', 'LOND F&B Revenue', 'LOND Rental Revenue', 'LOND AV Revenue', 'LOND Attendance', 'LOND ADR', 'LOND Peak RNs',
                             'Total Account Booking', 'Account Conversion', 'Total Agency Booking', 'Agency Conversion']
    columns_to_te = ['Account: Region', 'Account: Industry', 'Agency: Region', 'Agency: Industry', 'End User Region', 
                     'End User SIC', 'Booking Type', 'Market Segment', 'Lead Source']
    column_name = ['RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month', 'Arrival Day', 'Departure Month',
                   'Departure Day', 'Booked Month', 'Booked Day']
    column_name_v1 = ['Last Status Month', 'Last Status Day', 'Decision day']
    
    col = columns_to_standarize + columns_to_te + column_name
    
    col_v1 = columns_to_standarize + columns_to_te + column_name + column_name_v1

    return col, col_v1


def transforming_booking_data(transformer, booking_data: pd.DataFrame) -> pd.DataFrame:

    col, col_v1 = prediction_columns_name()
    
    data_tmp = pd.DataFrame(booking_data, columns=col)
    
    data_tmp = transformer.transform(data_tmp)
    data_tmp = pd.DataFrame(data_tmp, columns=col)
    data_tmp = data_tmp.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    data_tmp = data_tmp.astype(float)
    
    return data_tmp


def shap_plot(model, explainer_dataset, shap_data, BK_ID_no, booking_post_as, booking_info_data): 
    
    # Shap force-plot
#    explainer = shap.TreeExplainer(model, data=explainer_dataset, model_output='probability', link='logit')
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(explainer_dataset)
    shap.force_plot(explainer.expected_value, shap_values[-1, :], shap_data, matplotlib=True, text_rotation=15, show=False)
    
    # Add booking info table
    booking_info_data.dropna(axis=1, inplace=True)
    booking_info_data.drop(['Account', 'Id', 'LastStatusDate', 'Last Status Month', 'Last Status Day', 'Decision day'], axis=1, inplace=True)
    booking_info_data['Post As'] = booking_post_as

    booking_info_data['ArrivalDate'] = booking_info_data['ArrivalDate'].dt.strftime('%Y-%m-%d')
    booking_info_data['DepartureDate'] = booking_info_data['DepartureDate'].dt.strftime('%Y-%m-%d')
    booking_info_data['BookedDate'] = booking_info_data['BookedDate'].dt.strftime('%Y-%m-%d')
    # move Post As to front
    col = booking_info_data.pop('Post As')
    booking_info_data.insert(0, 'Post As', col)

    booking_info_data = (booking_info_data.T).reset_index()
    plt.table(cellText=booking_info_data.values, colLabels=[' ', 'Booking Info'], loc='bottom', colLoc='center', cellLoc='center', colWidths=[0.2]*2)
    
    # Save as pdf
    booking_post_as = re.sub('[^a-zA-Z0-9 \n\.]','', booking_post_as)
    plt.savefig(BK_ID_no + '_' + booking_post_as + '-forceplot.pdf', format='pdf', dpi=800, bbox_inches ='tight')
    plt.close()
    

# main function
def main() -> None:
    # input booking ID
    BK_ID_no = '019621'
#    BK_ID = 'a090I00001GrxPuQAJ'
    
    # shap dataset version
    model_name = '20230922'
    
    # Model file path
    model_path = 'H:\\Python scripts\\Machine Learning\\MICE\\'
    model_path2 = 'Error Analysis\\xgb\\'

    # property list
    property_list = {'The Venetian Macao': 'VMRH', 'Conrad Macao': 'CMCC', 'Holiday Inn Macao Cotai Central': 'HICC', 'The Parisian Macao': 'PARIS', 
                     'The Londoner Macao Hotel': 'LOND'}
    # data columns
    data_col = ['RNs', 'RNs Revenue', 'F&B Revenue', 'Rental Revenue', 'AV Revenue', 'Attendance', 'ADR', 'Peak RNs']
    
    # Create property column name for empty dataframe
    columns = ['Id']
    for key, val in property_list.items():
        for col in data_col:
            tmp = val + ' ' + col
            columns.append(tmp)
    
    sql_data = Extract_SQLServer_Data()
    
    # get current date
    current_date = sql_data.now
    
    raw_booking_data, BK_ID_no = sql_data.sql_ml_bk_data_id_no(BK_ID_no)
#    raw_booking_data, BK_ID_no = sql_data.sql_ml_bk_data_id(BK_ID)

    RoomB_data = sql_data.sql_ml_room_block_data()
    Event_data = sql_data.sql_ml_event_data()
    
    booking_post_as = raw_booking_data.iloc[0]['Post As']

    booking_data = raw_booking_data.drop(['Account: Name', 'Agency: Name', 'Post As', 'Owner'], axis=1)
    
    booking_data = create_ml_booking_dataframe(booking_data, RoomB_data, Event_data, property_list, columns)
    
    booking_data = booking_ac_ag_conversion_data(booking_data, model_path)

    booking_data = preprocessing_booking_data(booking_data, current_date)

    # load transformer
    transformer = open(model_path + model_path2 + '\\' + model_name + '\\' + model_name + '_xgb_materization_transformer.pkl', 'rb')
    transformer = pickle.load(transformer)
    
    # load model
    model = open(model_path + model_path2 + '\\' + model_name + '\\' + model_name + '_xgb_materization_model.pkl', 'rb')
    model = pickle.load(model)
    
    shap_data = transforming_booking_data(transformer, booking_data)

    explainer_dataset = pd.read_excel(model_name + '_xai_explainer_dataset.xlsx')
    
    explainer_dataset = pd.concat([explainer_dataset, shap_data], axis=0)

    explainer_dataset.reset_index(drop=True, inplace=True)

    shap_plot(model, explainer_dataset, shap_data, BK_ID_no, booking_post_as, booking_data)


main()
